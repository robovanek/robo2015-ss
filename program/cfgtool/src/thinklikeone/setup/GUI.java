package thinklikeone.setup;

import lejos.hardware.Button;
import lejos.hardware.lcd.GraphicsLCD;
import lejos.utility.Delay;
import thinklikeone.gui.Platform;
import thinklikeone.gui.Screen;

import java.io.File;
import java.io.IOException;

/**
 * GUI on EV3's display
 * TODO add key repeat
 */
public class GUI {
	private GraphicsLCD lcd;
	private FormActivity formActive = FormActivity.LIST;
	private Platform platform;
	private Screen screen;
	private GUIControls controls;
	private File confFile;


	public GUI(File confFile) {
		this.confFile = confFile;
		this.platform = Platform.getPlatform();
		this.platform.setImmediate(false);
		this.lcd = platform.getRenderer().getLCD();
		this.screen = platform.createDefaultScreen();
		this.controls = new GUIControls(lcd, screen);
	}

	public static void start(File confFile) throws IOException {
		GUI gui = new GUI(confFile);
		if (Main.EV3) {
			Button.LEDPattern(1);
		}
		gui.loop();
		if (Main.EV3) {
			Button.LEDPattern(0);
		}
	}

	/**
	 * Main loop
	 */
	public void loop() throws IOException {
		boolean quit = false;
		controls.showList();
		formActive = FormActivity.LIST;
		while (true) {
			platform.processEvents();
			if (controls.isClosed(formActive)) {
				switch (formActive) {
					case LIST:
						controls.hideList();
						if (controls.getList().exitSelected()) {
							controls.showSaveDialog();
							formActive = FormActivity.SAVE;
						} else {
							String name = controls.getList().getSelectedKey();
							Settings.Type t = Settings.getType(name);
							switch (t) {
								case STRING:
									controls.showStringEntry(name, Settings.getString(name));
									formActive = FormActivity.STRING_ENTRY;
									break;
								case FLOAT:
									controls.showFloatEntry(name, Settings.getFloat(name));
									formActive = FormActivity.FLOAT_ENTRY;
									break;
								case INT:
									controls.showIntEntry(name, Settings.getInt(name));
									formActive = FormActivity.INT_ENTRY;
									break;
								case BOOL:
									controls.showBoolEntry(name, Settings.getBool(name));
									formActive = FormActivity.BOOL_ENTRY;
									break;
							}
						}
						break;
					case STRING_ENTRY:
						setAndClose(controls.getStringEntry());
						break;
					case INT_ENTRY:
						if (controls.getIntEntry().isSwitched()) {
							String name = controls.getIntEntry().getName();
							Integer value = controls.getIntEntry().getValue();
							controls.showLongIntEntry(name, value);
							formActive = FormActivity.LONG_INT_ENTRY;
						} else {
							setAndClose(controls.getIntEntry());
						}
						break;
					case LONG_INT_ENTRY:
						setAndClose(controls.getLongEntry());
						break;
					case FLOAT_ENTRY:
						setAndClose(controls.getFloatEntry());
						break;
					case BOOL_ENTRY:
						setAndClose(controls.getBoolEntry());
						break;
					case SAVE:
						switch (controls.getSaveDialog().getValue()) {
							case CANCEL:
								controls.showList();
								formActive = FormActivity.LIST;
								break;
							case CLOSE:
								quit = true;
								break;
							case CLOSE_SAVE:
								Settings.save(confFile);
								quit = true;
								break;
						}
						if (!quit) {
							controls.showList();
							formActive = FormActivity.LIST;
						}
						break;
				}
			}
			if (quit)
				break;
			lcd.clear();
			screen.draw(0, 0);
			lcd.refresh();
			Delay.msDelay(10);
		}
	}

	private void setAndClose(Entry e) {
		if (e.valueChanged()) {
			String name = e.getName();
			Object value = e.getValue();
			Settings.set(name, value);
		}
		controls.showList();
		formActive = FormActivity.LIST;
	}

	public enum FormActivity {
		LIST, STRING_ENTRY, INT_ENTRY, LONG_INT_ENTRY, FLOAT_ENTRY, BOOL_ENTRY, SAVE
	}
}