package thinklikeone.setup;

import thinklikeone.gui.SwingGraphicsLCD;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * Panel for rendering in {@link thinklikeone.gui.SwingGraphicsLCD}
 *
 * @author Jakub Vaněk {@literal <vanek.jakub4@seznam.cz>}
 */
public class EV3SwingPanel extends JPanel {
	public static final Color PIXEL_WHITE = new Color(0, 190, 18);
	public static final Color PIXEL_BLACK = Color.BLACK;
	public BufferedImage img;
	public SwingGraphicsLCD lcd;
	public JFrame frame;
	public Image scaled;

	public EV3SwingPanel(JFrame frame) {
		this.frame = frame;
		img = new BufferedImage(178, 128, BufferedImage.TYPE_3BYTE_BGR);
		Graphics g = img.getGraphics();
		g.setColor(PIXEL_WHITE);
		g.fillRect(0, 0, 178, 128);
		g.dispose();
		setBackground(PIXEL_WHITE);/*
	    Timer t = new Timer(500, new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				repaint();
			}
		});
		t.setRepeats(true);
		t.setCoalesce(true);
		t.start();*/
	}

	public SwingGraphicsLCD getLCD() {
		return lcd;
	}

	public void setLCD(SwingGraphicsLCD lcd) {
		this.lcd = lcd;
	}

	@Override
	public Dimension getPreferredSize() {
		return new Dimension(178, 128);
	}

	@Override
	protected void paintComponent(Graphics g) {
		synchronized (img) {
			int xscale = frame.getWidth() / 178;
			int yscale = frame.getHeight() / 128;
			int scale = Math.min(xscale, yscale);
			if (scale < 1)
				scale = 1;
			Image localImage;
			if (scale != 1) {
				scaled = localImage = img.getScaledInstance(178 * scale, 128 * scale, Image.SCALE_REPLICATE);
			} else {
				localImage = img;
			}
			Dimension d = new Dimension(178 * scale, 128 * scale);
			//if (frame.getSize().width < 178 * scale || frame.getSize().height < 128 * scale)
			//	frame.setSize(d);
			if (getSize().width < 178 * scale || getSize().height < 128 * scale)
				setSize(d);
			g.drawImage(localImage, 0, 0, 178 * scale, 128 * scale, null);
		}
	}
}
