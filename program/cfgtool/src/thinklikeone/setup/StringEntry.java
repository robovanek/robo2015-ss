package thinklikeone.setup;

import lejos.hardware.lcd.GraphicsLCD;
import thinklikeone.gui.Event;
import thinklikeone.gui.KeyCode;
import thinklikeone.gui.KeyboardEvent;
import thinklikeone.gui.Platform;

import java.awt.event.KeyEvent;

/**
 * Dialog for entering strings
 *
 * @author Jakub Vaněk {@literal <vanek.jakub4@seznam.cz>}
 */
public class StringEntry extends Entry<String> {
	private static final int MAX_CHARS = 17;
	private static StringEntry instance;
	private int curPos = 0;
	private int viewPos = 0;
	private String original = "";
	private StringBuilder value = null;

	public StringEntry(GraphicsLCD surface) {
		this.lcd = surface;
		this.value = new StringBuilder();
	}

	@Override
	public void draw(int x, int y) {
		//clear();
		// draw title
		drawName(x, y);
		// draw contents
		lcd.drawRect(1 + x, Main.SMALL_FONT.height + 3 + y, MAX_CHARS * Main.NORMAL_FONT.width + 3, Main.NORMAL_FONT.height + 4);
		lcd.setFont(Main.NORMAL_FONT);
		int endIndex = viewPos + MAX_CHARS > value.length() ?
				value.length() : viewPos + MAX_CHARS;
		lcd.drawString(value.toString().substring(viewPos, endIndex), 3 + x, Main.SMALL_FONT.height + 6 + y, TOP_LEFT);
		// draw cursor
		int lineX = (curPos - viewPos) * Main.NORMAL_FONT.width + 2;
		int lineBaseY = Main.SMALL_FONT.height + 5;
		lcd.drawLine(lineX + x, lineBaseY + y, lineX + x, lineBaseY + y + Main.NORMAL_FONT.height);
	}

	@Override
	public void open(String value, String name) {
		this.value.setLength(0);
		this.value.append(value);
		this.original = value;
		setDefaultPos();
		this.name = name;
		this.closed = false;
		Platform.getPlatform().getKeyboard().setPhysical(false);
		Platform.getPlatform().getKeyboard().activate();
	}

	@Override
	public String getValue() {
		return value.toString();
	}

	@Override
	public void setValue(String value) {
		this.value.setLength(0);
		this.value.append(value);
		this.original = value;
		setDefaultPos();
	}

	@Override
	public boolean valueChanged() {
		return !original.equals(value.toString());
	}

	private void setDefaultPos() {
		viewPos = 0;
		curPos = value.length() > MAX_CHARS ? 0 : value.length();
	}

	private void curLeft() {
		if (curPos > 0)
			curPos--;
		if (curPos - viewPos < 0)
			viewPos--;
	}

	private void curRight() {
		if (curPos < value.length())
			curPos++;
		if (curPos - viewPos > MAX_CHARS)
			viewPos++;
	}

	private void backspace() {
		if (curPos > 0)
			value.deleteCharAt(curPos - 1);
		curLeft();
	}

	private void accept() {
		closed = true;
		Platform.getPlatform().getKeyboard().deactivate();
	}

	private void decline() {
		this.value.setLength(0);
		this.value.append(original);
		closed = true;
		Platform.getPlatform().getKeyboard().deactivate();
	}

	@Override
	public void eventHandler(Event event) {
		try {
			if (event instanceof KeyboardEvent) {
				KeyboardEvent<KeyCode> kbdEvent = (KeyboardEvent<KeyCode>) event;
				if (kbdEvent.getType() == KeyboardEvent.Type.PRESS) {
					switch (kbdEvent.getKey()) {
						case ACCEPT:
							accept();
							break;
						case EXIT:
							decline();
							break;
						case BACKSPACE:
							backspace();
							break;
						case LEFT:
							curLeft();
							break;
						case RIGHT:
							curRight();
							break;
						default:
							if (isPrintableChar(kbdEvent.getCharacter())) {
								value.insert(curPos, kbdEvent.getCharacter());
								curRight();
							}
							break;
					}
				}
			}
		} catch (ClassCastException e) {
			System.err.println("BUG! " + e.getMessage());
		}
	}

	public boolean isPrintableChar(char c) {
		Character.UnicodeBlock block = Character.UnicodeBlock.of(c);
		return c != (char) 0 &&
				(!Character.isISOControl(c)) &&
				c != KeyEvent.CHAR_UNDEFINED &&
				block != null &&
				block != Character.UnicodeBlock.SPECIALS;
	}
}