package thinklikeone.setup;

import lejos.hardware.lcd.GraphicsLCD;
import thinklikeone.gui.Screen;

/**
 * Singletonizer for controls.
 */
public class GUIControls {
	private ListContent list;
	private IntEntry intEntry;
	private FloatEntry floatEntry;
	private LongIntEntry longEntry;
	private BoolEntry boolEntry;
	private StringEntry stringEntry;
	private SaveEntry saveDialog;
	private Screen screen;
	private Screen.Layer listLayer;
	private Screen.Layer entryLayer;

	public GUIControls(GraphicsLCD lcd, Screen screen) {
		this.screen = screen;
		Header head = new Header();
		screen.setHeader(head);
		listLayer = screen.getLayer(0, 0, 0f, lcd.getWidth(), lcd.getHeight() - head.getHeight());
		entryLayer = screen.getLayer(0, 0, 1f, lcd.getWidth(), lcd.getHeight() - head.getHeight());
		list = new ListContent(lcd);
		listLayer.getContainer().addControl(list);
		intEntry = new IntEntry(lcd);
		floatEntry = new FloatEntry(lcd);
		longEntry = new LongIntEntry(lcd);
		boolEntry = new BoolEntry(lcd);
		stringEntry = new StringEntry(lcd);
		saveDialog = new SaveEntry(lcd);
	}

	public ListContent getList() {
		return list;
	}

	public IntEntry getIntEntry() {
		return intEntry;
	}

	public FloatEntry getFloatEntry() {
		return floatEntry;
	}

	public LongIntEntry getLongEntry() {
		return longEntry;
	}

	public BoolEntry getBoolEntry() {
		return boolEntry;
	}

	public StringEntry getStringEntry() {
		return stringEntry;
	}

	public SaveEntry getSaveDialog() {
		return saveDialog;
	}

	public Screen getScreen() {
		return screen;
	}

	public Screen.Layer getListLayer() {
		return listLayer;
	}

	public Screen.Layer getEntryLayer() {
		return entryLayer;
	}

	public void clearEntryLayer() {
		entryLayer.getContainer().clearControls();
	}

	public void showList() {
		clearEntryLayer();
		entryLayer.setTransparent(true);
		entryLayer.setInputIgnore(true);
		list.prepareShow();
	}

	public void hideList() {
		entryLayer.setTransparent(false);
		entryLayer.setInputIgnore(false);
	}

	protected void putEntry(Entry e, String name, Object value) {
		clearEntryLayer();
		entryLayer.getContainer().addControl(e);
		e.open(value, name);
	}

	public void showIntEntry(String name, int value) {
		putEntry(intEntry, name, value);
	}

	public void showLongIntEntry(String name, int value) {
		putEntry(longEntry, name, value);
	}

	public void showFloatEntry(String name, float value) {
		putEntry(floatEntry, name, value);
	}

	public void showBoolEntry(String name, boolean value) {
		putEntry(boolEntry, name, value);
	}

	public void showStringEntry(String name, String value) {
		putEntry(stringEntry, name, value);
	}

	public void showSaveDialog() {
		putEntry(saveDialog, "Exit action", SaveEntry.SaveResult.CLOSE_SAVE);
	}

	public boolean isClosed(GUI.FormActivity form) {
		switch (form) {
			case LIST:
				return list.isClosed();
			case STRING_ENTRY:
				return stringEntry.isClosed();
			case INT_ENTRY:
				return intEntry.isClosed();
			case LONG_INT_ENTRY:
				return longEntry.isClosed();
			case FLOAT_ENTRY:
				return floatEntry.isClosed();
			case BOOL_ENTRY:
				return boolEntry.isClosed();
			case SAVE:
				return saveDialog.isClosed();
		}
		throw new IllegalArgumentException("form is not a legal value!");
	}
}
