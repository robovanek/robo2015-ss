package thinklikeone.setup;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * Class for settings manipulation
 *
 * @author Jakub Vaněk {@literal <vanek.jakub4@seznam.cz>}
 */
public abstract class Settings {
	private static Properties properties;
	private static Map<String, Type> types;

	/**
	 * Load a file into static storage.
	 *
	 * @param file Name of the settings file.
	 * @throws IOException Passed through from {@link Files#readAllLines}.
	 */
	public static void load(File file) throws IOException {
		properties = new Properties();
		try (FileInputStream stream = new FileInputStream(file)) {
			properties.load(stream);
		}
		types = new HashMap<>(properties.size());
		for (Map.Entry entry : properties.entrySet()) {
			String key = (String) entry.getKey();
			String value = (String) entry.getValue();
			if (value.toLowerCase().equals("true") || value.toLowerCase().equals("false")) {
				types.put(key, Type.BOOL);
				continue;
			}
			try {
				Integer.parseInt(value);
				types.put(key, Type.INT);
				continue;
			} catch (NumberFormatException e) {
			}
			try {
				Float.parseFloat(value);
				types.put(key, Type.FLOAT);
				continue;
			} catch (NumberFormatException e) {
			}
			types.put(key, Type.STRING);
		}
	}

	public static void save(File file) throws IOException {
		try (FileOutputStream stream = new FileOutputStream(file)) {
			properties.store(stream, "Saved configuration from ThinkLikeOne Setup Utility");
		}
	}

	/**
	 * Get the type of value with specified key.
	 *
	 * @param key Unique string key.
	 * @return Type of value.
	 */
	public static Type getType(String key) {
		return types.get(key);
	}

	/**
	 * Get float value with specified key.
	 *
	 * @param key Unique string key.
	 * @return Float value if the type of key is float, if not, {@link Float#NaN} is returned.
	 */
	public static float getFloat(String key) {
		if (isType(key, Type.FLOAT) || isType(key, Type.INT)) {
			return Float.parseFloat(properties.getProperty(key));
		} else
			return Float.NaN;
	}

	/**
	 * Set float value with specified key to a specified value.
	 *
	 * @param key   Unique string key.
	 * @param value Value to set.
	 * @throws IllegalArgumentException if type of value with key isn't {@link Type#FLOAT}.
	 */
	public static void setFloat(String key, float value) {
		if (isType(key, Type.FLOAT)) {
			properties.setProperty(key, Float.toString(value));
		} else
			throw new IllegalArgumentException("Type of value with key \"" + key + "\" is not a float.");
	}

	/**
	 * Get integer value with specified key.
	 *
	 * @param key Unique string key.
	 * @return Integer value if the type of key is integer, if not, {@code 0} is returned.
	 */
	public static int getInt(String key) {
		if (isType(key, Type.INT))
			return Integer.parseInt(properties.getProperty(key));
		else
			return 0;
	}

	/**
	 * Set integer value with specified key to a specified value.
	 *
	 * @param key   Unique string key.
	 * @param value Value to set.
	 * @throws IllegalArgumentException if type of value with key isn't {@link Type#INT}.
	 */
	public static void setInt(String key, int value) {
		if (isType(key, Type.INT)) {
			properties.setProperty(key, Integer.toString(value));
		} else if (isType(key, Type.FLOAT)) {
			properties.setProperty(key, Float.toString(value));
		} else {
			throw new IllegalArgumentException("Type of value with key \"" + key + "\" is not a float or integer.");
		}
	}

	/**
	 * Get string value with specified key.
	 *
	 * @param key Unique string key.
	 * @return String representation of value with specified key (e.g. this will always work).
	 */
	public static String getString(String key) {
		return properties.getProperty(key);
	}

	/**
	 * Set string value with specified key to a specified value.
	 *
	 * @param key   Unique string key.
	 * @param value Value to set.
	 * @throws IllegalArgumentException if type of value with key isn't {@link Type#STRING}.
	 */
	public static void setString(String key, String value) {
		if (isType(key, Type.STRING)) {
			properties.setProperty(key, value);
		} else {
			throw new IllegalArgumentException("Type of value with key \"" + key + "\" is not a float.");
		}
	}

	/**
	 * Get boolean value with specified key.
	 *
	 * @param key Unique string key.
	 * @return Boolean value if the type of key is boolean, if not, {@code false} is returned.
	 */
	public static boolean getBool(String key) {
		return isType(key, Type.BOOL) && Boolean.parseBoolean(properties.getProperty(key));
	}


	/**
	 * Set boolean value with specified key to a specified value.
	 *
	 * @param key   Unique string key.
	 * @param value Value to set.
	 * @throws IllegalArgumentException if type of value with key isn't {@link Type#BOOL}.
	 */
	public static void setBool(String key, Boolean value) {
		if (isType(key, Type.BOOL)) {
			properties.setProperty(key, Boolean.toString(value));
		} else {
			throw new IllegalArgumentException("Type of value with key \"" + key + "\" is not a float.");
		}
	}

	public static void set(String key, Object value) {
		properties.setProperty(key, value.toString());
	}

	public static Object[] keys() {
		return properties.keySet().toArray();
	}

	/**
	 * Check value type.
	 *
	 * @param key  Unique string key
	 * @param type Specified value type.
	 * @return Whether the type of the value with the specified key is the same as the specified type.
	 */
	public static boolean isType(String key, Type type) {
		return getType(key) == type;
	}

	public static int size() {
		return properties.size();
	}

	/**
	 * Soft value type enumeration.
	 */
	public enum Type {
		/**
		 * String type, used by default.
		 */
		STRING,
		/**
		 * Single-precision floating point type
		 */
		FLOAT,
		/**
		 * Integer type
		 */
		INT,
		/**
		 * Boolean type
		 */
		BOOL
	}
}