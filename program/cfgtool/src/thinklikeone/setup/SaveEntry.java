package thinklikeone.setup;


import lejos.hardware.lcd.GraphicsLCD;
import thinklikeone.gui.Event;
import thinklikeone.gui.KeyCode;
import thinklikeone.gui.KeyboardEvent;
import thinklikeone.gui.Platform;

/**
 * Save dialog
 */
public class SaveEntry extends Entry<SaveEntry.SaveResult> {
	private static final int MAX_POS = 2;
	private GraphicsLCD lcd;
	private SaveResult original;
	private SaveResult value;
	private int pos;

	public SaveEntry(GraphicsLCD lcd) {
		this.lcd = lcd;
	}

	@Override
	public void open(SaveResult value, String name) {
		this.closed = false;
		pos = 0;
		for (int i = 0; i < SaveResult.values().length; i++) {
			if (SaveResult.values()[i] == value) {
				pos = i;
				break;
			}
		}
		this.name = name;
		this.original = value;
		this.value = value;
		Platform.getPlatform().getKeyboard().setPhysical(true);
		Platform.getPlatform().getKeyboard().activate();
	}

	@Override
	public SaveResult getValue() {
		return value;
	}

	@Override
	public void setValue(SaveResult value) {
		this.value = value;
	}

	@Override
	public boolean valueChanged() {
		return original != value;
	}

	@Override
	public void draw(int x, int y) {
		drawName(x, y);
		int baseX = x + Main.SMALL_FONT.height + 3;
		int baseY = y + 10;
		int shift = Main.NORMAL_FONT.height + 6;
		lcd.setFont(Main.NORMAL_FONT);
		drawButton(baseX, baseY, SaveResult.CLOSE_SAVE.name, pos == 0);
		drawButton(baseX, baseY + shift, SaveResult.CLOSE.name, pos == 1);
		drawButton(baseX, baseY + shift * 2, SaveResult.CANCEL.name, pos == 2);
	}

	private void accept() {
		closed = true;
		Platform.getPlatform().getKeyboard().deactivate();
	}

	private void decline() {
		value = original;
		closed = true;
		Platform.getPlatform().getKeyboard().deactivate();
	}

	private void keyUp() {
		if (pos > 0) {
			pos--;
		} else {
			pos = MAX_POS;
		}
	}

	private void keyDown() {
		if (pos < MAX_POS) {
			pos++;
		} else {
			pos = 0;
		}
	}

	private void select() {
		switch (pos) {
			case 0:
				value = SaveResult.CLOSE_SAVE;
				break;
			case 1:
				value = SaveResult.CLOSE;
				break;
			case 2:
				value = SaveResult.CANCEL;
				break;
		}
	}

	@Override
	public void eventHandler(Event event) {
		KeyboardEvent<KeyCode> k = KeyboardEvent.castToKeyboardEvent(event);
		if (k == null || k.getType() == KeyboardEvent.Type.RELEASE)
			return;
		switch (k.getKey()) {
			case PHYS_UP:
				keyUp();
				break;
			case PHYS_DOWN:
				keyDown();
				break;
			case PHYS_ENTER:
				select();
				accept();
				break;
			case PHYS_ESCAPE:
				value = SaveResult.CANCEL;
				accept();
				break;
		}
	}

	private void drawButton(int x, int y, String text, boolean selected) {
		int baseW = text.length() * Main.NORMAL_FONT.width;
		int baseH = Main.NORMAL_FONT.height;
		lcd.drawRect(x, y, baseW + 3, baseH + 3);
		if (selected)
			lcd.fillRect(x + 2, y + 2, baseW, baseH);
		lcd.drawString(text, x + 2, y + 2, GraphicsLCD.TOP | GraphicsLCD.LEFT, selected);
	}

	public enum SaveResult {
		CLOSE_SAVE("Close + save"), CLOSE("Just Close"), CANCEL("Cancel");
		private String name;

		SaveResult(String prompt) {
			this.name = prompt;
		}
	}
}
