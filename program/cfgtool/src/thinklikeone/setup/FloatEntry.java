package thinklikeone.setup;

import lejos.hardware.lcd.GraphicsLCD;
import thinklikeone.gui.*;

import java.math.BigDecimal;

/**
 * Dialog for entering strings
 *
 * @author Jakub Vaněk {@literal <vanek.jakub4@seznam.cz>}
 */
public class FloatEntry extends Entry<Float> {
	private static final int MAX_CHARS = 17;
	private float original = 0;
	private StringBuilder value = null;
	private boolean hasDot = false;
	private boolean negative = false;
	private int curPos = 0;
	private int viewPos = 0;

	public FloatEntry(GraphicsLCD surface) {
		this.lcd = surface;
		this.value = new StringBuilder();
	}

	private static String padLeft(String s, int n) {
		// source: http://stackoverflow.com/questions/388461/how-can-i-pad-a-string-in-java
		return String.format("%1$" + n + "s", s);
	}

	@Override
	public void open(Float value, String name) {
		setValue(value);
		this.name = name;
		this.closed = false;
		Keyboard kbd = Platform.getPlatform().getKeyboard();
		if (kbd instanceof EV3Keyboard) {
			EV3Keyboard ev3kbd = (EV3Keyboard) kbd;
			ev3kbd.setLayout(new NumberKeyboardLayout());
		}
		kbd.setPhysical(false);
		kbd.activate();
	}

	@Override
	public Float getValue() {
		try {
			return Float.parseFloat(value.toString()) * (negative ? -1 : 1);
		} catch (Exception e) {
			return 0f;
		}
	}

	@Override
	public void setValue(Float value) {
		this.value.setLength(0);
		this.value.append(BigDecimal.valueOf(Math.abs(value)).toPlainString());
		negative = Math.signum(value) == -1;
		hasDot = this.value.indexOf(".") != -1;
		this.original = value;
		setDefaultPos();
	}

	@Override
	protected void drawName(int x, int y) {
		lcd.setFont(Main.SMALL_FONT);
		String prompt = name + ":" + "(sign: " + (negative ? "-" : "+") + ")";
		lcd.drawString(prompt, 2 + x, 1 + y, TOP_LEFT);
	}

	@Override
	public boolean valueChanged() {
		return original != getValue();
	}

	private void setDefaultPos() {
		viewPos = 0;
		curPos = 0;
		//curPos = value.length() > MAX_CHARS ? 0 : value.length();
	}

	private void curLeft() {
		if (curPos > 0)
			curPos--;
		if (curPos - viewPos < 0)
			viewPos--;
	}

	private void curRight() {
		if (curPos < value.length())
			curPos++;
		if (curPos - viewPos > MAX_CHARS)
			viewPos++;
	}

	private void backspace() {
		if (curPos > 0) {
			if (value.charAt(curPos - 1) == '.')
				hasDot = false;
			value.deleteCharAt(curPos - 1);
			curLeft();
			if (viewPos > 0 && curPos == viewPos) {
				viewPos--;
			}
		}
	}

	private void accept() {
		closed = true;
		deconfigureKbd();
	}

	private void decline() {
		this.value.setLength(0);
		this.value.append(original);
		closed = true;
		deconfigureKbd();
	}

	private void deconfigureKbd() {
		Keyboard kbd = Platform.getPlatform().getKeyboard();
		kbd.deactivate();
		if (kbd instanceof EV3Keyboard) {
			EV3Keyboard ev3kbd = (EV3Keyboard) kbd;
			ev3kbd.unsetLayout();
		}
	}

	@Override
	public void draw(int x, int y) {
		// draw title
		drawName(x, y);
		// draw border
		lcd.drawRect(1 + x, Main.SMALL_FONT.height + 3 + y, lcd.getWidth() - 3, Main.NORMAL_FONT.height + 4);
		// draw text
		lcd.setFont(Main.NORMAL_FONT);
		String str = padLeft(value.toString(), MAX_CHARS);
		int spaces = Math.max(MAX_CHARS - value.length(), 0);
		if (value.length() > MAX_CHARS) {
			int end = viewPos + MAX_CHARS > value.length() ?
					value.length() : viewPos + MAX_CHARS;
			str = str.substring(viewPos, end);
		}
		lcd.drawString(str, 3 + x, Main.SMALL_FONT.height + 6 + y, TOP_LEFT);
		// draw cursor
		int lineX = (curPos - viewPos + spaces) * Main.NORMAL_FONT.width + 2;
		int lineBaseY = Main.SMALL_FONT.height + 5;
		lcd.drawLine(lineX + x, lineBaseY + y, lineX + x, lineBaseY + y + Main.NORMAL_FONT.height);
	}

	@Override
	public void eventHandler(Event event) {
		try {
			if (event instanceof KeyboardEvent) {
				KeyboardEvent<KeyCode> kbdEvent = (KeyboardEvent<KeyCode>) event;
				if (kbdEvent.getType() == KeyboardEvent.Type.PRESS) {
					if (kbdEvent.getKey().isSpecial()) {
						switch (kbdEvent.getKey()) {
							case BACKSPACE:
								backspace();
								break;
							case ACCEPT:
								accept();
								break;
							case EXIT:
								decline();
								break;
							case LEFT:
								curLeft();
								break;
							case RIGHT:
								curRight();
								break;
						}
					} else {
						char ch = kbdEvent.getKey().getChar();
						if (Character.isDigit(ch)) {
							value.insert(curPos, ch);
							curRight();
						} else if (ch == '-') {
							negative = !negative;
						} else if (ch == '.') {
							if (!hasDot) {
								value.insert(curPos, '.');
								hasDot = true;
								curRight();
							}
						}
					}
				}
			}
		} catch (ClassCastException e) {
			System.err.println("BUG! " + e.getMessage());
		}
	}
}