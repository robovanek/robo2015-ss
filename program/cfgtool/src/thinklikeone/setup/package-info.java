/**
 * ThinkLikeOne Setup Utility
 *
 * @version 0.1
 * @author Jakub Vaněk {@literal <vanek.jakub4@seznam.cz>}
 * @author Matej Kripner <kripnermatej@gmail.com>
 */
package thinklikeone.setup;