package main;

/**
 * <p>Class representing possible robot operations (movements & sensors) in actual environment.</p>
 * <p/>
 * <p>All units of length are supposed to be millimeters.</p>
 *
 * @author Matej Kripner <kripnermatej@gmail.com>
 * @author Jakub Vaněk {@literal vanek.jakub4@seznam.cz}
 * @version 1.0
 */
public abstract class Robotics {
	private static Robotics robotics = null;

	/**
	 * Get internal Robotics instance.
	 *
	 * @return Implementation of physics.
	 * @throws IllegalStateException Thrown when instance wasn't set yet-
	 */
	public static Robotics getRobotics() {
		if (robotics == null) throw new IllegalStateException("robotics not set yet");
		return robotics;
	}

	/**
	 * Set internal Robotics instance.
	 *
	 * @param robotics Initialized implementation of physics.
	 */
	public static void setRobotics(Robotics robotics) {
		Robotics.robotics = robotics;
	}

	/**
	 * Move the robot forward at set velocity.
	 *
	 * @param millimeters How far should robot go in millimeters.
	 * @param block       If this function will block or not.
	 */
	public abstract void moveForward(int millimeters, boolean block);

	/**
	 * Move the robot backward at set velocity.
	 *
	 * @param millimeters How far should robot go in millimeters.
	 * @param block       If this function will block or not.
	 */
	public abstract void moveBackward(int millimeters, boolean block);

	/**
	 * Start moving the robot forward (don't stop after distance) at set velocity.
	 */
	public abstract void startMoveForward();

	/**
	 * Start moving the robot backward (don't stop after distance) at set velocity.
	 */
	public abstract void startMoveBackward();

	/**
	 * Stop moving the robot.
	 */
	public abstract void stop();

	/**
	 * Rotate the robot along Y axis
	 *
	 * @param degrees Rotation in degrees. Positive is counter-clockwise, negative is clockwise.
	 * @param block   If this function will block or not.
	 */
	public abstract void rotate(int degrees, boolean block);

	/**
	 * Get the velocity for moving the robot along straight line.
	 *
	 * @return Velocity in millimeters per second (mm/s)
	 */
	public abstract float getMoveVelocity();

	/**
	 * Set the velocity for moving the robot along straight line.
	 *
	 * @param mm_per_sec Velocity in millimeters per second (mm/s)
	 */
	public abstract void setMoveVelocity(float mm_per_sec);

	/**
	 * Set the acceleration for moving the robot along straight line.
	 *
	 * @param mm_per_sec_squared Acceleration in millimeters per second squared (mm/s^2)
	 */
	public abstract void setMoveAcceleration(float mm_per_sec_squared);

	/**
	 * Get the velocity for rotating robot along it's Y axis.
	 *
	 * @return Angular velocity in degrees per second (°/s)
	 */
	public abstract float getRotationVelocity();

	/**
	 * Set the velocity for rotating robot along it's Y axis.
	 *
	 * @param deg_per_sec Angular velocity in degrees per second (°/s)
	 */
	public abstract void setRotationVelocity(float deg_per_sec);

	/**
	 * Set the acceleration for rotating robot along it's Y axis.
	 *
	 * @param deg_per_sec_squared Angular acceleration in degrees per second squared (°/s^2)
	 */
	public abstract void setRotationAcceleration(float deg_per_sec_squared);

	/**
	 * Check if the touch sensor in the front side of the robot is pressed.
	 *
	 * @return {@code true} if pressed, {@code false} if released.
	 */
	public abstract boolean frontTouch();

	/**
	 * Get the distance reported by the ultrasonic sensor in the left side of the robot.
	 *
	 * @return Distance in millimeters. <b>If distance is out of sensor's range, the value is {@link Float#POSITIVE_INFINITY}</b>
	 */
	public abstract float leftSonic();

	/**
	 * Get the actual robot heading.
	 *
	 * @return Angle in degrees (°)
	 */
	public abstract float gyro();

	/**
	 * Calibrate the gyroscope. Note: this is time-consuming blocking operation.
	 */
	public abstract void calibrateGyro();

	/**
	 * Get the offset that is <b>subtracted</b> from measured heading.
	 *
	 * @return Offset in degrees (°)
	 */
	public abstract float getGyroOffset();

	/**
	 * Set the offset that will be <b>subtracted</b> from measured heading.
	 * This can be used as soft-reset of gyroscope (i.e. without calibration)
	 *
	 * @param value Offset in degrees (°)
	 */
	public abstract void setGyroOffset(float value);

	/**
	 * Put the magnet down.
	 *
	 * @param block If this function will block or not.
	 */
	public abstract void magnetDown(boolean block);

	/**
	 * Put the magnet up. This function doesn't block.
	 *
	 * @param block If this function will block or not.
	 */
	public abstract void magnetUp(boolean block);

	public abstract boolean stoppedByTouch();

	/**
	 * Detect the light under the robot's color sensor.
	 *
	 * @return {@link main.Robotics.Light#OTHER} or {@link main.Robotics.Light#RED}.
	 */
	public abstract Light detectLight();

	/**
	 * Start moving the robot towards the wall, until a touch sensor gets pressed.
	 *
	 * @param block Whether this function will block or not.
	 */
	public abstract void moveToWall(boolean block);

	/**
	 * Check if the robot is moving now.
	 *
	 * @return {@code true} if it's moving, {@code false} otherwise.
	 */
	public abstract boolean isMoving();

	/**
	 * Get the distance travelled by the left wheel of the robot during last move.
	 *
	 * @return Distance in millimeters (mm)
	 */
	public abstract float getDistanceTravelledLeft();

	/**
	 * Disable motor power.
	 */
	public abstract void free();

	/**
	 * Get the distance travelled by the right wheel of the robot during last move.
	 *
	 * @return Distance in millimeters (mm)
	 */
	public abstract float getDistanceTravelledRight();

	/**
	 * Flush any cached actions.
	 */
	public abstract void flush();

	/**
	 * Wait for stop of actual move.
	 */
	public abstract void waitForStop();

	/**
	 * Light measurement enumeration
	 */
	public enum Light {
		/**
		 * Red light
		 */
		RED,
		/**
		 * Other (we cannot differentiate between colors)
		 */
		OTHER
	}
}
