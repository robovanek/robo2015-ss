package main.robot.ai;

import main.robot.ai.map.Direction;
import main.robot.ai.map.Point;

/**
 * @author Matej Kripner <kripnermatej@gmail.com>; google.com/+MatejKripner
 */
public class Robot {
	private static Robot instance = new Robot();

	private Point location;
	private Direction direction;

	private Robot() {
		reset();
	}

	public static Robot getRobot() {
		return instance;
	}

	public void reset() {
		location = new Point(4, 3);
		direction = Direction.NORTH;
	}

	public Direction getDirection() {
		return direction;
	}

	public void setDirection(Direction direction) {
		this.direction = direction;
	}

	public Point getLocation() {
		return location;
	}

	public void setLocation(Point location) {
		this.location = location;
	}

	public Point nextIn(Way way) {
		return inDirection(way, 1);
	}

	public Point inDirection(Way way, int blocks) {
		Direction target = null;
		if (way == Way.HEAD) target = direction;
		else if (way == Way.LEFT) target = direction.rotateLeft();
		else if (way == Way.BACK) target = direction.rotateLeft().rotateLeft();
		else if (way == Way.RIGHT) target = direction.rotateRight();
		return location.moveInDirection(target, blocks);
	}


	public enum Way {
		HEAD,
		LEFT,
		BACK,
		RIGHT
	}
}
