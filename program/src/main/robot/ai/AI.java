package main.robot.ai;

import main.robot.ai.map.Direction;
import main.robot.ai.map.Field;
import main.robot.ai.map.Map;
import main.robot.ai.map.Point;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.Random;

import static main.robot.ai.Movement.getMovement;
import static main.robot.ai.Robot.Way;

/**
 * Primary robot's algorithm
 *
 * @author Matej Kripner <kripnermatej@gmail.com>
 * @version 0.1
 */
public class AI implements Runnable {
	private static final long RUN_LENGTH = 90 * 1000;

	private long startMillis = -1;
	private int moves = 0;

	private Way last, lastLast;

	@Override
	public void run() {
		Map.getMap().reset();
		Robot.getRobot().reset();
		Movement.getMovement().prepare();
		startMillis = System.currentTimeMillis();
		fromStart(); // basic routine
		blind(); // the actual algorithm
	}

	private void fromStart() {
		getMovement().exploreForward(Direction.EAST);
//		getMovement().turnRight();
//		getMovement().exploreForward();
	}

	/**
	 * The actual algorithm. Do its best until the thread is interrupted.
	 */
	private void blind() {
		while (!Thread.currentThread().isInterrupted()) {
			// align
			Map.Unit front = Map.getMap().get(Robot.getRobot().getLocation().nextInDirection(Robot.getRobot().getDirection()));
			if (front.getWithHighestProbability() == Field.WALL && ++moves > 5) {
				Movement.getMovement().exploreForward();
				Map.getMap().somethingWrong(front.getHighestProbability());
				moves = 0;
			}
			Path best = bestMove();
			Way next = best == null ? Way.values()[(int) (Math.random() * Way.values().length)] : Robot.getRobot().getDirection().wayTo(best.getFirst()); // if no result (quite bad), go on random
			if(lastLast == next) {
				next = Robot.getRobot().getDirection().rotate((int) (Math.random() * 4)).wayTo(Direction.EAST);
			}
			lastLast = last;
			last = next;
			Direction preferredEnd = (best == null || best.getPath().size() < 2) ? Robot.getRobot().getDirection().rotateRight() : best.getPath().get(1);
			if(!getMovement().go(next, preferredEnd)) moves = 0;
		}
	}

	/**
	 * Find the best robot's move for the actual robot's position & direction on the actual map.
	 *
	 * @return the best move
	 */
	private Path bestMove() {
		PathFinder pathFinder = new PathFinder();
		pathFinder.run(3); // base run
		return pathFinder.findMostAttractive();
	}

	/**
	 * Get remaining time in milliseconds.
	 *
	 * @return Number of milliseconds elapsed from start.
	 */
	public long timeRemaining() {
		return startMillis == -1 ? Long.MAX_VALUE : RUN_LENGTH - (System.currentTimeMillis() - startMillis);
	}

	/**
	 * Used for finding the best path for the actual robot's position & direction on the actual map.
	 *
	 * @author Matej Kripner <kripnermatej@gmail.com>
	 * @version 0.1
	 */
	private static class PathFinder {
		// the paths that are currently being calculated by this finder
		private LinkedList<Path> paths = new LinkedList<>();
		// paths that should be replaced from this list to paths list
		private LinkedList<Path> pathsToAdd = new LinkedList<>();
		// flag used to determine if an attractive path wasn't found and so we should be more careful
		private boolean critical = false;
		// the best (most attractive) path
		private Path best = null;

		public PathFinder() {
			// the initial path
			paths.add(new Path(Robot.getRobot().getLocation(), Robot.getRobot().getDirection()));
		}

		public synchronized Path findMostAttractive() {
			while (bestPath() == null || bestPath().getAttractiveness() <= 0)
				next(); // keep searching until we have a suitable solution
			if (bestPath() == null) { // not found
				if (critical) return null; // critical mode was already set on without result
				else {
					critical = true;
					return findMostAttractive(); // try it again with the critical flag
				}
			}
			return bestPath();
		}

		/**
		 * Run the searching algorithm with depth determined by the argument.
		 *
		 * @param times depth of the algorithm
		 */
		public synchronized void run(int times) {
			for (int i = 0; i < times; i++) if (!next()) return;
		}

		/**
		 * Try to find next step for each path.
		 *
		 * @return true if at least one next step of one path was found; false otherwise
		 */
		public synchronized boolean next() {
			boolean somethingDone = false;
			for (Iterator<Path> iterator = paths.iterator(); iterator.hasNext(); ) {
				Path path = iterator.next();
				if (explore(path)) {
					somethingDone = true;
					iterator.remove(); // remove the path (a next step was added)
				}
			}
			flushPaths(); // manage the pathsToAdd list
			if (somethingDone) best = null; // reset the best path
			return somethingDone;
		}

		private void flushPaths() {
			paths.addAll(pathsToAdd);
			pathsToAdd.clear();
		}

		/**
		 * Find and return the best path that is currently being calculated. This does no sense unless the {@link
		 * #next()}, {@link #findMostAttractive()} or {@link #run()} method is called.
		 *
		 * @return the best path
		 */
		public synchronized Path bestPath() {
			if (best != null) return best;
			Path best = null;
			for (Path path : paths) {
				if (best == null || best.compareTo(path) < -100) best = path;
			}
			this.best = best;
			return best;
		}

		// try to find next step for the given path.
		private boolean explore(Path path) {
			boolean somethingDone = false; // determine if at least one suitable next step of the path was found
			for (Direction direction : Direction.values()) { // examine each direction
				Point point = path.getEndPoint().nextInDirection(direction);
				Map.Unit unit = Map.getMap().get(point);
				// if it is probable that we shouldn't move in that direction and critical mode is not on, continue
				if (unit.getWithHighestProbability() == Field.WALL && unit.isResponsible() && !(critical && unit.getHighestProbability() != 1))
					continue;
				Path newPath = path.add(direction, point); // try to create a next step of the path in actually examined direction
				if (newPath == null) continue; // not found
				pathsToAdd.add(newPath); // add the new path with next step
				somethingDone = true;
			}
			return somethingDone;
		}
	}

	/**
	 * Class representing a path that would the robot theoretically go through.
	 *
	 * @author Matej Kripner <kripnermatej@gmail.com>
	 * @version 0.1
	 */
	private static class Path {
		// Set of instructions that create this path
		private final LinkedList<Direction> path;
		// 3 last points of this path. Use for determining useless going back
		private Point[] lasts = new Point[3];
		// attractiveness of this path
		private double attractiveness;
		// the point, where would the robot theoretically end
		private Point endPoint;
		// the direction, in which would the robot theoretically end
		private Direction endDirection;

		public Path(Point endPoint, Direction actualDirection) {
			this.endPoint = endPoint;
			this.endDirection = actualDirection;
			this.path = new LinkedList<>();
		}

		private Path(LinkedList<Direction> path, Point endPoint, Point[] lasts, double attractiveness, Direction actualDirection) {
			this.path = path;
			this.endPoint = endPoint;
			this.lasts = lasts;
			this.attractiveness = attractiveness;
			this.endDirection = actualDirection;
		}

		/**
		 * Find out if the given direction is suitable to be the next in this path. Return a path with that direction
		 * added if yes, null otherwise.
		 *
		 * @param direction the direction that's being examined
		 * @param endPoint  the point from which would we move in the direction
		 * @return a path with the given direction added if that direction is suitable to be the next in this path; null
		 * otherwise
		 */
		public Path add(Direction direction, Point endPoint) {
			if (lasts[2] != null && lasts[2].equals(endPoint)) return null; // useless going back
			Point[] newLasts = nextLasts(endPoint); // updated 3 last points of the resulted path
			LinkedList<Direction> newPath = new LinkedList<>(path); // "cloned" set of instructions
			newPath.add(direction);

			return new Path(newPath, endPoint, newLasts, computeAttractiveness(direction, endPoint), direction);
		}

		// compute the attractiveness of the given direction
		private double computeAttractiveness(Direction direction, Point endPoint) {
			// basic math - sum of actual attractiveness and the end point attractiveness
			double newAttractiveness = attractiveness + Map.getMap().get(endPoint).getAttractiveness();
			// if US will be useful
			Point onLeft = endPoint.nextInDirection(direction.rotateLeft());
			if (Map.getMap().get(onLeft).getWithHighestProbability() == Field.UNKNOWN) attractiveness += 800;
			// how difficult it is to travel in that direction
			if (direction == endDirection) attractiveness += 150;
			else if (direction == endDirection.rotate(2)) attractiveness -= 150;

			return newAttractiveness;
		}

		// return updated 3 last points of the resulted path in respect to the given one
		private Point[] nextLasts(Point next) {
			Point[] newLasts = new Point[lasts.length];
			newLasts[2] = lasts[1];
			newLasts[1] = lasts[0];
			newLasts[0] = next;
			return newLasts;
		}

		public Point getEndPoint() {
			return endPoint;
		}

		public double getAttractiveness() {
			return attractiveness;
		}

		public Direction getFirst() {
			return path.getFirst();
		}

		public double compareTo(Path o) {
			return getAttractiveness() - o.getAttractiveness();
		}

		public LinkedList<Direction> getPath() {
			return path;
		}
	}
}
