package main.robot.ai.map;

import java.util.Objects;

/**
 * @author Matej Kripner <kripnermatej@gmail.com>; google.com/+MatejKripner
 */
public class Point {
	public final int x;
	public final int y;

	public Point(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public Point moveInDirection(Direction direction, int blocks) {
		if (direction == Direction.NORTH) return new Point(x, y - blocks);
		if (direction == Direction.WEST) return new Point(x - blocks, y);
		if (direction == Direction.SOUTH) return new Point(x, y + blocks);
		if (direction == Direction.EAST) return new Point(x + blocks, y);
		return null;
	}

	public Point nextInDirection(Direction direction) {
		return moveInDirection(direction, 1);
	}

	public int getY() {
		return y;
	}

	public int getX() {
		return x;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Point point = (Point) o;
		return Objects.equals(x, point.x) && // to lazy to change this auto-implemented shit
				Objects.equals(y, point.y);
	}

	@Override
	public int hashCode() {
		return Objects.hash(x, y);
	}
}
