package main.robot.ai.map;

import main.robot.ai.Robot;

/**
 * Robot orientation enumeration
 *
 * @author Matej Kripner <kripnermatej@gmail.com>; google.com/+MatejKripner
 * @version 1.0
 */
public enum Direction {
	NORTH,
	WEST,
	SOUTH,
	EAST;

	/**
	 * Rotate this Direction to right by specified number of 90° turns.
	 *
	 * @param toRight Number of turns to right. Negative values mean turning left.
	 * @return Direction after turn
	 */
	public Direction rotate(int toRight) {
		int r = toRight % values().length;
		if (r > 0 && toRight < 0)
			r -= values().length;
		int newIndex = (ordinal() + r + values().length) % values().length;
		return values()[newIndex];
	}

	public Direction rotateLeft() {
		return values()[(ordinal() + 1) % values().length];
	}

	public Direction rotateRight() {
		return values()[(ordinal() + values().length - 1) % values().length];
	}

	public Robot.Way wayTo(Direction target) {
		if (target == this) {
			return Robot.Way.HEAD;
		} else if (target == rotateLeft()) {
			return Robot.Way.LEFT;
		} else if (target == rotateRight()) {
			return Robot.Way.RIGHT;
		} else if(target == rotate(2)) {
			return Robot.Way.BACK;
		} else throw new IllegalStateException("WTF");
	}
}
