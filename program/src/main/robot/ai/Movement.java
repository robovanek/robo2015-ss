package main.robot.ai;

import main.Robotics;
import main.Settings;
import main.robot.ai.map.Direction;
import main.robot.ai.map.Map;
import main.robot.ai.map.Point;

import static main.Robotics.getRobotics;
import static main.robot.ai.Robot.getRobot;

/**
 * Provides and high-level operations
 *
 * @author Matej Kripner <kripnermatej@gmail.com>
 * @version 0.1
 */
public class Movement {
	private static Movement movement;

	public static Movement getMovement() {
		return movement == null ? (movement = new Movement()) : movement;
	}

	public void prepare() {
		getRobotics().magnetDown(false);
	}

	private boolean afterFireFight = false;

	/**
	 * Go forward. If crash into a wall, go back to the middle of a block.
	 *
	 * @param blocks number of blocks to go
	 * @return how many bocks were actually gone by the robot
	 */
	public int exploreForward(int blocks) {
		for (int i = 0; i < blocks; i++) {
			if (!exploreForward()) {
				return i;
			}
		}
		return blocks;
	}

	/**
	 * Go forward. If crash into a wall, go back to the middle of a block.
	 *
	 * @return true if the robot didn't crash; false otherwise
	 */
	public boolean exploreForward(Direction preferredEndDirection) {
		Point point = getRobot().nextIn(Robot.Way.HEAD); // the end point
		Map.Unit unit = Map.getMap().get(point);
		// try to move forward
		if (!tryForward()) {
			// edit the map with the new info about the wall
			unit.checkWall(Settings.FLOAT.TOUCH_PRECISION.getValue());
			unit.setProbabilities(new double[] {0, 1, 0, 0, 0}, Settings.FLOAT.TOUCH_PRECISION.getValue());
			return false;
		}
		unit.checkSpace(Settings.FLOAT.TOUCH_PRECISION.getValue()); // check if we should panic (actual and stored values differ)
		getRobot().setLocation(point); // update the info about the robot
		ultrasonic(); // do the US measurement
		// checks if we should use the magnet TODO: do the light detection every time if we aren't going to use light searching
		if (detectLight() == Robotics.Light.RED) {
			Point nextPoint = getRobot().nextIn(Robot.Way.HEAD);
			Map.Unit nextUnit = Map.getMap().get(nextPoint);

			// try to go forward to use the magnet
			getRobotics().magnetDown(false);
			boolean moved = moveForward(Settings.INT.WHEELS_TO_MAGNET_DOWN.getValue());
			if (!moved) { // there is a wall
//				getRobotics().moveBackward(Settings.INT.WHEELS_TO_HEAD.getValue(), true);
//				turnAndMagnet(getRobot().getDirection().wayTo(preferredEndDirection));
				//nextUnit.checkWall(Settings.FLOAT.TOUCH_PRECISION.getValue());
				nextUnit.setProbabilities(new double[] {0, 1, 0, 0, 0}, Settings.FLOAT.TOUCH_PRECISION.getValue());
			} else {
//				getRobotics().moveBackward(Settings.INT.WHEELS_TO_MAGNET_DOWN.getValue(), true);
			}
			afterFireFight = moved;
		}
		unit.setProbabilities(new double[] {0, 0, 1, 0, 0}, Settings.FLOAT.TOUCH_PRECISION.getValue()); // store info about the space
		getRobotics().magnetUp(false);
		return true;
	}

	public boolean exploreForward() {
		return exploreForward(getRobot().getDirection().rotate(2));
	}

	// use only if there is a wall in front of us!
	private void turnAndMagnet(Robot.Way endDirection) {
		Direction turn1, turn2, turn3;
		if (endDirection == Robot.Way.BACK || endDirection == Robot.Way.HEAD) {
			turn1 = getRobot().getDirection().rotate(2);
			turn2 = getRobot().getDirection().rotateRight();
			turn3 = getRobot().getDirection().rotateLeft();
		} else if (endDirection == Robot.Way.LEFT) {
			turn1 = getRobot().getDirection().rotateLeft();
			turn2 = turn1.rotateLeft();
			turn3 = turn2.rotateLeft();
		} else {
			turn1 = getRobot().getDirection().rotateRight();
			turn2 = turn1.rotateRight();
			turn3 = turn2.rotateRight();
		}
		Point point1 = getRobot().getLocation().nextInDirection(turn1), point2 = getRobot().getLocation().nextInDirection(turn2);
		Map.Unit unit1 = Map.getMap().get(point1), unit2 = Map.getMap().get(point2);
		turnTo(getRobot().getDirection().wayTo(turn1));
		if (!moveForward(Settings.INT.WHEELS_TO_MAGNET_DOWN.getValue())) {
			unit1.checkWall(Settings.FLOAT.TOUCH_PRECISION.getValue());
			unit1.setProbabilities(new double[] {0, 1, 0, 0, 0}, Settings.FLOAT.TOUCH_PRECISION.getValue());
//			getRobotics().moveBackward(Settings.INT.WHEELS_TO_HEAD.getValue(), true);
			turnTo(getRobot().getDirection().wayTo(turn2));
			if (!moveForward(Settings.INT.WHEELS_TO_MAGNET_DOWN.getValue())) { // magnet should reach the light
				unit2.checkWall(Settings.FLOAT.TOUCH_PRECISION.getValue());
				unit2.setProbabilities(new double[] {0, 1, 0, 0, 0}, Settings.FLOAT.TOUCH_PRECISION.getValue());
//				getRobotics().moveBackward(Settings.INT.WHEELS_TO_HEAD.getValue(), true);
				turnTo(getRobot().getDirection().wayTo(turn3));
				moveForward(Settings.INT.WHEELS_TO_MAGNET_DOWN.getValue());
			}

		}
	}

	// tries to go forward. If crash, return to the center of the unit
	private boolean tryForward() {
		boolean moved;
		if (afterFireFight) {
			moved = moveForward(Settings.INT.FIELD_SIZE.getValue() - Settings.INT.WHEELS_TO_MAGNET_DOWN.getValue());
			afterFireFight = false;
		} else moved = moveForward(Settings.INT.FIELD_SIZE.getValue());
		if (!moved) { // return to the center of the unit
//			getRobotics().moveBackward(Settings.INT.WHEELS_TO_HEAD.getValue(), true);
			return false;
		}
		return true;
	}

	/**
	 * @param millimeters
	 * @return Whether the robot has moved.
	 */
	private boolean moveForward(final int millimeters) {
		getRobotics().moveForward(millimeters, true);
		getRobotics().flush();
		return !getRobotics().stoppedByTouch();
	}

	// tries to detect the light by moving in different directions
	private Robotics.Light detectLight() {
		Robotics.Light light = getRobotics().detectLight();
		if (light == Robotics.Light.RED) return light;
		if (!Settings.BOOLEAN.CRAZY_LIGHT_FINDER.getValue())
			return Robotics.Light.OTHER;
		try {
			// this just ... sort of happened
			getRobotics().moveForward(Settings.INT.LIGHT_SIZE.getValue(), true);
			tryLight();
//			getRobotics().moveBackward(Settings.INT.LIGHT_SIZE.getValue() * 2, true);
			tryLight();
			getRobotics().rotate(45, true);
			int forward = (int) Math.sqrt(2 * Settings.INT.LIGHT_SIZE.getValue() * Settings.INT.LIGHT_SIZE.getValue());
			getRobotics().moveForward(forward, true);
			tryLight();
//			getRobotics().moveBackward(forward, true);
			getRobotics().rotate(-90, true);
			getRobotics().moveForward(forward, true);
			tryLight();
//			getRobotics().moveBackward(forward, true);
			getRobotics().rotate(45, true);
		} catch (RedFound e) {
			return Robotics.Light.RED;
		}
		return Robotics.Light.OTHER;
	}

	// help method for the detectLight()
	private void tryLight() throws RedFound {
		Robotics.Light light = getRobotics().detectLight();
		if (light == Robotics.Light.RED) throw new RedFound();
	}

	public void turnLeft() {
		if (afterFireFight) {
			getRobotics().moveBackward(Settings.INT.WHEELS_TO_MAGNET_DOWN.getValue(), true);
			afterFireFight = false;
		}
		getRobotics().rotate(90, true);
		getRobot().setDirection(getRobot().getDirection().rotateLeft());
		ultrasonic();
	}

	public void turnRight() {
		if (afterFireFight) {
			getRobotics().moveBackward(Settings.INT.WHEELS_TO_MAGNET_DOWN.getValue(), true);
			afterFireFight = false;
		}
		getRobotics().rotate(-90, true);
		getRobot().setDirection(getRobot().getDirection().rotateRight());
		ultrasonic();
	}

	/**
	 * Go in the given way.
	 *
	 * @param way way to go
	 */
	public boolean go(Robot.Way way, Direction preferredEndDirection) {
		if (way == Robot.Way.RIGHT) turnRight();
		else if (way == Robot.Way.LEFT) turnLeft();
		else if (way == Robot.Way.BACK) turnBack();
		else return exploreForward(preferredEndDirection);
		return true;
	}

	public void turnTo(Robot.Way way) {
		if (way == Robot.Way.BACK) turnBack();
		else if (way == Robot.Way.RIGHT) turnRight();
		else if (way == Robot.Way.LEFT) turnLeft();
	}

	public void turnBack() {
		if (afterFireFight) {
			getRobotics().moveBackward(Settings.INT.WHEELS_TO_MAGNET_DOWN.getValue(), true);
			afterFireFight = false;
		}
		getRobotics().rotate(180, true);
		getRobot().setDirection(getRobot().getDirection().rotate(2));
		ultrasonic();
	}

	// do the US measurement
	private void ultrasonic() {
		float millis = getRobotics().leftSonic();
		int blocks = Float.isInfinite(millis) ? Settings.INT.MAX_SONIC_BLOCKS.getValue() : (int) (millis / Settings.INT.FIELD_SIZE.getValue());
		for (int i = 1; i <= blocks; i++) {
			// edit each measured unit probability
			Map.Unit actual = Map.getMap().get(getRobot().inDirection(Robot.Way.LEFT, i));
			if (!actual.getWithHighestProbability().isSpace()) // if this unit isn't already measured
				actual.setProbabilities(new double[] {0, 0, 0.5, 0.5, 1}, Settings.FLOAT.SONIC_PRECISION.getValue());
		}
		if (Float.isInfinite(millis)) return; // no end block
		Map.Unit end = Map.getMap().get(getRobot().inDirection(Robot.Way.LEFT, blocks + 1)); // the end point (first wall)
		if (!end.isOutOfBounds()) { // it makes sense to set probabilities
			double wallProbability = (end.getWallProbability() + Settings.FLOAT.SONIC_PRECISION.getValue() * 6) / 7 - ((0.1 * blocks * (1 - Settings.FLOAT.SONIC_PRECISION.getValue())));
			end.checkWall(wallProbability);
			end.setProbabilities(new double[] {1 - wallProbability, wallProbability, 0, 0, 0}, Settings.FLOAT.SONIC_PRECISION.getValue());
		}
	}

	/**
	 * Efficient exception that has no stacktrace; we use this for flow-control.
	 */
	static final class RedFound extends Exception {
		RedFound() {
		}

		@Override
		public Throwable fillInStackTrace() {
			return this;
		}
	}
}
