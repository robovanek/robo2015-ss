package main.robot;

import lejos.robotics.RegulatedMotor;
import main.Settings;

/**
 * <p>Two-and-half-wheel robot controller<p/>
 * TODO: Pedantic debugging of each line of code - this is critical part of code. If this part fails, nothing will work.
 *
 * @author Jakub Vaněk {@literal vanek.jakub4@seznam.cz}
 * @version 1.0
 **/
public class SuperPilot {
	private final RegulatedMotor leftMotor;
	private final RegulatedMotor rightMotor;
	private final float trackWidth;
	private final double leftDegreesPerMillimeter;
	private final double rightDegreesPerMillimeter;
	private float speed;
	private float acceleration;
	private Regulator regulator;

	/**
	 * Creates new SuperPilot instance. This class assumes that it has exclusive access to the motors.
	 *
	 * @param leftMotor          Left motor
	 * @param rightMotor         Right motor
	 * @param leftWheelDiameter  Diameter of the left wheel of the robot in millimeters.
	 * @param rightWheelDiameter Diameter of the right wheel of the robot in millimeters.
	 * @param trackWidth         Distance between centers of robot's wheels in millimeters.
	 * @throws IllegalStateException if {@link Sensors} weren't initialized.
	 */
	public SuperPilot(RegulatedMotor leftMotor, RegulatedMotor rightMotor,
	                  float leftWheelDiameter, float rightWheelDiameter, float trackWidth) {
		// check sensors instance
		if (Sensors.getInstance() == null)
			throw new IllegalStateException("Sensors aren't initialized");
		// passthrough
		this.leftMotor = leftMotor;
		this.rightMotor = rightMotor;
		this.leftMotor.synchronizeWith(new RegulatedMotor[] {this.rightMotor});
		this.trackWidth = trackWidth;
		// calculate degrees/mm conversion ratio
		this.leftDegreesPerMillimeter = 360D / (leftWheelDiameter * Math.PI);
		this.rightDegreesPerMillimeter = 360D / (rightWheelDiameter * Math.PI);
		// initialize regulator
		this.regulator = new Regulator();
		// set default speed/accel
		setSpeed(Settings.INT.FORWARD_SPEED.getValue());
		setAcceleration(Settings.INT.FORWARD_ACCEL.getValue());
	}

	/**
	 * Get the speed of the robot
	 *
	 * @return Speed in millimeters per second (mm/s)
	 */
	public float getSpeed() {
		return speed;
	}

	/**
	 * Set the speed of the robot
	 *
	 * @param speed Speed in millimeters per second (mm/s)
	 */
	public void setSpeed(float speed) {
		this.speed = speed;
		regulator.setSpeed(speed, speed);
	}

	/**
	 * Get the acceleration of robot
	 *
	 * @return Acceleration in millimeters per second squared (mm/s^2)
	 */
	public float getAcceleration() {
		return acceleration;
	}

	/**
	 * Set the acceleration of robot
	 *
	 * @param acceleration Acceleration in millimeters per second squared (mm/s^2)
	 */
	public void setAcceleration(float acceleration) {
		this.acceleration = acceleration;
		setAcceleration();
	}

	/**
	 * Calculate and set acceleration for each motor
	 */
	private void setAcceleration() {
		int leftAcceleration = (int) Math.round(leftDegreesPerMillimeter * acceleration);
		int rightAcceleration = (int) Math.round(rightDegreesPerMillimeter * acceleration);
		leftMotor.startSynchronization();
		leftMotor.setAcceleration(leftAcceleration);
		rightMotor.setAcceleration(rightAcceleration);
		leftMotor.endSynchronization();
	}

	/**
	 * Move the robot in a straight line
	 *
	 * @param distance    Distance to travel in millimeters. Negative values mean that the robot will move backward.
	 * @param block       Whether this function will return immediately or not.
	 * @param ignoreTouch Whether the regulator should ignore pressed touch sensor. Useful for travelback.
	 */
	public void travel(int distance, boolean block, boolean ignoreTouch) {
		stopRegulator();
		waitForRegulatorStop();
		regulator.setupTravel(distance, ignoreTouch);
		startRegulator(!block);
	}

	/**
	 * Check whether motors are moving.
	 *
	 * @return {@code true} if the left or the right motor is moving, {@code false} otherwise.
	 */
	public boolean isMoving() {
		return isLeftMoving() || isRightMoving();
	}

	/**
	 * Check whether the left motor is moving.
	 *
	 * @return {@code true} if the left motor is moving, {@code false} otherwise.
	 */
	private boolean isLeftMoving() {
		return leftMotor.isMoving();
	}

	/**
	 * Check whether the right motor is moving.
	 *
	 * @return {@code true} if the right motor is moving, {@code false} otherwise.
	 */
	private boolean isRightMoving() {
		return rightMotor.isMoving();
	}

	/**
	 * Start moving the robot forward in a straight line, don't stop after defined distance.
	 *
	 * @param touch_stop Whether the motors should stop after touch sensor is pressed.
	 */
	public void forward(boolean touch_stop, boolean block) {
		stopRegulator();
		waitForRegulatorStop();
		regulator.setupFreeTravel(false, touch_stop);
		startRegulator(!block);
	}

	/**
	 * Stop the regulator and motors.
	 *
	 * @param block Whether this function waits for the stop.
	 */
	public void stop(boolean block) {
		regulator.stopEnd = true;
		stopRegulator();
		if (block) {
			waitForRegulatorStop();
		}
	}

	/**
	 * Wait for regulator stop.
	 */
	protected void waitForRegulatorStop() {
		if (regulator != null) {
			try {
				while (regulator.isRunning()) {
					synchronized (regulator.wait_lock) {
						regulator.wait_lock.wait();
					}
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Stop the regulator.
	 */
	private void stopRegulator() {
		if (regulator.isRunning())
			regulator.interrupt();
	}

	/**
	 * Start moving the robot backward in a straight line, don't stop after defined distance.
	 */
	public void backward(boolean block) {
		stopRegulator();
		waitForRegulatorStop();
		regulator.setupFreeTravel(true, true);
		startRegulator(!block);
	}

	/**
	 * Rotate the robot by specified angle.
	 *
	 * @param angle How many degrees must the robot turn.
	 * @param block Whether this function will block or not.
	 */
	public void rotate(final float angle, final boolean block) {
		stopRegulator();
		waitForRegulatorStop();
		regulator.setupTurn(angle);
		startRegulator(!block);
	}

	/**
	 * Start the regulator.
	 *
	 * @param async Whether the regulator will run in its own thread.
	 */
	private void startRegulator(boolean async) {
		if (!async) {
			regulator.run();
			return;
		}
		Thread regThread = new Thread(regulator, "Regulator");
		regThread.setPriority(Thread.MAX_PRIORITY);
		regThread.setDaemon(false);
		regThread.start();
	}

	/**
	 * Get the distance travelled by the right wheel during last move operation.
	 *
	 * @return Distance travelled by the right wheel in millimeters.
	 */
	public float getDistanceTravelledRight() {
		return (rightMotor.getTachoCount() - regulator.src_right) / (float) rightDegreesPerMillimeter;
	}

	/**
	 * Get the distance travelled by the left wheel during last move operation.
	 *
	 * @return Distance travelled by the left wheel in millimeters.
	 */
	public float getDistanceTravelledLeft() {
		return (leftMotor.getTachoCount() - regulator.src_left) / (float) leftDegreesPerMillimeter;
	}

	/**
	 * Disable motor hold.
	 *
	 * @param block Whether this function will block or not.
	 */
	public void flt(boolean block) {
		leftMotor.startSynchronization();
		leftMotor.flt(true);
		rightMotor.flt(true);
		leftMotor.endSynchronization();
		if (block) {
			leftMotor.waitComplete();
			rightMotor.waitComplete();
		}
	}

	/**
	 * Get expected gyro angle obtained by the addition of other angles.
	 *
	 * @return Expected gyro angle in degrees.
	 */
	public float getExpectedGyro() {
		return regulator.getExpectedGyro();
	}

	/**
	 * Check if last regulator run was stopped by touch sensor,
	 *
	 * @return {@code true} if stopped, {@code false} if not.
	 */
	public boolean stoppedByTouch() {
		return regulator.stoppedByTouch();
	}

	/**
	 * Regulator mode
	 *
	 * @author Jakub Vaněk {@literal vanek.jakub4@seznam.cz}
	 * @version 2.0
	 */
	public enum RegulatorMode {
		FREE_TRAVEL,
		ARC_TURN,
		TURN,
		TRAVEL
	}

	/**
	 * Wheel synchronization and movement correction
	 *
	 * @author Jakub Vaněk {@literal vanek.jakub4@seznam.cz}
	 * @version 2.0
	 */
	public class Regulator implements Runnable {
		protected final float turn_ratio;
		protected final Object wait_lock;
		protected RegulatorMode mode;
		protected float argument = 0;
		protected int delta_left = 0;
		protected int delta_right = 0;
		protected volatile boolean interrupt;
		protected int src_left;
		protected int src_right;
		protected float angle = 0f;
		protected volatile boolean running = false;
		protected boolean stopEnd;
		protected volatile boolean stoppedByTouch;
		protected boolean travel_ignoreTouch;

		/**
		 * Default constructor
		 */
		public Regulator() {
			// calculate turn ratio
			turn_ratio = (float) (Math.PI * trackWidth / 360);
			// initialize wait lock
			wait_lock = new Object();
		}

		/**
		 * Get expected gyro angle obtained by the addition of other angles.
		 *
		 * @return Expected gyro angle in degrees.
		 */
		public float getExpectedGyro() {
			return angle;
		}

		/**
		 * Set the motors on for free travel, update PWM.
		 */
		protected void freetravel_activate() {
			leftMotor.startSynchronization();
			if (argument == 1) { // if we should go forward
				leftMotor.forward();
				rightMotor.forward();
			} else {
				leftMotor.backward();
				rightMotor.backward();
			}
			leftMotor.endSynchronization();
		}

		/**
		 * Set motor speeds
		 *
		 * @param left  Speed of the left motor in mm/s
		 * @param right Speed of the right motor in mm/s
		 */
		protected void setSpeed(float left, float right) {
			leftMotor.startSynchronization();
			leftMotor.setSpeed((int) (left * leftDegreesPerMillimeter));
			rightMotor.setSpeed((int) (right * rightDegreesPerMillimeter));
			leftMotor.endSynchronization();
		}

		/**
		 * Run the regulator. Exactly one setup functions must be called before regulator is run.
		 */
		@Override
		public void run() {
			// set the running flag
			running = true;
			// set source tachometer values
			src_left = leftMotor.getTachoCount();
			src_right = rightMotor.getTachoCount();
			// reset the interrupt flag
			interrupt = false;
			switch (mode) { // switch on mode
				case FREE_TRAVEL: // free travel
					// set speed
					setSpeed(speed, speed);
					// set the motors on
					freetravel_activate();
					while (!interrupt) { // while we aren't interrupted
						// record loop start time
						long old_time = System.nanoTime();
						if (Sensors.getInstance().measureTouch()) { // if the touch sensor is pressed
							// set the touch stop flag
							stoppedByTouch = true;
							// load the correct angle
							angle = Sensors.getInstance().measureAngle();
							break;
						}
						// synchronize wheels by speed modification
						travelRegulator_tick();
						// update PWM
						freetravel_activate();
						// sleep
						sleep(old_time);
					}
					break;
				case ARC_TURN: // arc turn mode
					// set speed
					setSpeed(speed, speed);
					if (delta_right != 0) { // turn right
						// while we aren't interrupted and we're moving
						while (!interrupt && rightMotor.isMoving()) {
							// record loop start time
							long old_time = System.nanoTime();
							// calculate new destination from gyro reading
							delta_right = (int) Math.round(turn_getRemainingAngle(angle) * rightDegreesPerMillimeter);
							// set source tacho to present value
							src_right = rightMotor.getTachoCount();
							// update PWM
							rightMotor.rotateTo(src_right + delta_right);
							// sleep
							sleep(old_time);
						}
					} else if (delta_left != 0) { // turn left
						// while we aren't interrupted and we're moving
						while (!interrupt && leftMotor.isMoving()) {
							// record loop start time
							long old_time = System.nanoTime();
							// calculate new destination from gyro reading
							delta_left = (int) Math.round(-turn_getRemainingAngle(angle) * leftDegreesPerMillimeter);
							// set source tacho to present value
							src_left = leftMotor.getTachoCount();
							// update PWM
							leftMotor.rotateTo(src_left + delta_left);
							// sleep
							sleep(old_time);
						}
					}
					break;
				case TURN: // normal turn mode
				{// set speed
					setSpeed(speed, speed);
					if (argument != 0) // if we are not just correcting
						turn_exec(argument); // start rotation
					// loop until the error is too small
					float gyro = Sensors.getInstance().measureAngle();
					float error = getExpectedGyro() - gyro;
					while (Math.abs(error) > Settings.FLOAT.TURN_MAX_ERROR.getValue()) {
						// set present tacho values
						src_left = leftMotor.getTachoCount();
						src_right = rightMotor.getTachoCount();
						// update rotation
						turn_exec(error * Settings.FLOAT.TURN_KP.getValue());
						// set new values for new iteration
						gyro = Sensors.getInstance().measureAngle();
						error = getExpectedGyro() - gyro;
					}
				}
				break;
				case TRAVEL: { // straight line move mode
					// set speed
					setSpeed(speed, speed);
					// set the motors on
					travel_activate();
					while (!(interrupt || travel_isStopped(true))) { // while we're not interrupted and we're still moving
						// record loop start time
						long old_time = System.nanoTime();
						// if we don't ignore touch and the touch is pressed
						if (!travel_ignoreTouch && Sensors.getInstance().measureTouch()) {
							// set touch stop flag
							stoppedByTouch = true;
							// adjust expected angle
							float gyro = Sensors.getInstance().measureAngle();
							float expect = getExpectedGyro();
							// if the offset is not too big
							if (Math.abs(expect - gyro) < Settings.INT.REGULATOR_MAX_FIX_OFFSET.getValue()) {
								// set its
								angle = gyro;
							}
							break;
						}
						// synchronize wheels
						travelRegulator_tick();
						// update PWM
						travel_activate();
						// sleep
						sleep(old_time);
					}
					break;
				}
			}
			// if we must stop at the end
			if (stopEnd) {
				// stop motors
				stopMotors();
			}
			// set speed to original value
			setSpeed(speed, speed);
			// unset running flag
			running = false;
			// send message about stop
			synchronized (wait_lock) {
				wait_lock.notifyAll();
			}
		}

		/**
		 * Rotate by the specified angle.
		 *
		 * @param degrees Angle to rotate by.
		 */
		protected void turn_exec(float degrees) {
			// calculate delta tachometer values
			double rot_dist = degrees * turn_ratio;
			delta_left = (int) Math.round(-rot_dist * leftDegreesPerMillimeter);
			delta_right = (int) Math.round(rot_dist * rightDegreesPerMillimeter);
			// loop until we're not interrupted or we're stopped
			boolean moving = false; // ignore first iteration
			while (!(interrupt || (travel_isStopped(true) && moving))) {
				// record loop start time
				long old_time = System.nanoTime();
				// calculate delta tachometers (firstly in degrees, then in millimeters)
				int left_delta_tacho = leftMotor.getTachoCount() - src_left;
				int left_delta_dist = (int) Math.round(left_delta_tacho / leftDegreesPerMillimeter);
				int right_delta_tacho = rightMotor.getTachoCount() - src_right;
				int right_delta_dist = (int) Math.round(right_delta_tacho / leftDegreesPerMillimeter);
				// calculate wheel offset
				int delta_dist_diff = right_delta_dist + left_delta_dist;
				// if the error is too small
				if (Math.abs(delta_dist_diff) < Settings.INT.REGULATOR_NOOP_LIMIT.getValue())
					delta_dist_diff = 0; // set it to zero
				// HACK // if the right wheel is rotating forward, multiply offset by -1
				if (delta_right < 0)
					delta_dist_diff *= -1;
				// set speed from tachometer offset
				setSpeedFromDiff(delta_dist_diff);
				// update PWM
				leftMotor.startSynchronization();
				leftMotor.rotateTo(Math.round(src_left + delta_left), true);
				rightMotor.rotateTo(Math.round(src_right + delta_right), true);
				leftMotor.endSynchronization();
				// don't ignore stopped motor check next time
				moving = true;
				// sleep
				sleep(old_time);
			}
		}

		/**
		 * Check if regulator was stopped by a touch sensor press.
		 *
		 * @return {@code true} if it was, {@code false} if it wasn't.
		 */
		protected boolean stoppedByTouch() {
			return stoppedByTouch;
		}

		/**
		 * Set motor speed from tachometer difference.
		 *
		 * @param right_more How much the right wheel is in front.
		 */
		protected void setSpeedFromDiff(int right_more) {
			// declare speed variables, default is the normal speed.
			float speed_left = speed;
			float speed_right = speed;
			// HACK //if we have reversed travel
			if ((mode == RegulatorMode.FREE_TRAVEL && argument == -1) ||
					(mode == RegulatorMode.TRAVEL && (delta_left < 0 || delta_right < 0)))
				right_more *= -1; // multiply difference by -1
			// if the difference is not zero
			if (right_more != 0) {
				// calculate new speed for the previously faster wheel
				float new_speed = speed - Math.abs(right_more) * Settings.FLOAT.REGULATOR_KP.getValue();
				// clip the speed to the lower bound
				if (new_speed < Settings.FLOAT.REGULATOR_MIN_SPEED_PERCENT.getValue() * speed)
					new_speed = Settings.FLOAT.REGULATOR_MIN_SPEED_PERCENT.getValue() * speed;
				// assing speed to correct wheel
				if (right_more > 0) { // slow down right wheel
					speed_right = Math.round(new_speed);
				} else { // delta_dist_diff < 0; slow down left wheel
					speed_left = Math.round(new_speed);
				}
			}
			// apply speed
			setSpeed(speed_left, speed_right);
		}

		/**
		 * Calculate remaining rotation angle from gyro value.
		 *
		 * @param target_gyro Target gyro value.
		 * @return Remaining angle.
		 */
		protected float turn_getRemainingAngle(float target_gyro) {
			float gyro = Sensors.getInstance().measureAngle();
			float delta = target_gyro - gyro;
			return delta * turn_ratio;
		}

		/**
		 * Slow down the loop. Sleep the defined time minus time spent in the loop.
		 *
		 * @param old_time Iteration start time.
		 */
		protected void sleep(long old_time) {
			try {
				Thread.sleep(Math.max(
						Math.round(Settings.INT.REGULATOR_SLEEP_MILLIS.getValue() -
								(System.nanoTime() - old_time) / 1_000_000d), 0));
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		/**
		 * Set the motors on for travel, update PWM.
		 */
		protected void travel_activate() {
			leftMotor.startSynchronization();
			leftMotor.rotateTo(src_left + delta_left, true);
			rightMotor.rotateTo(src_right + delta_right, true);
			leftMotor.endSynchronization();
		}

		/**
		 * Check if motors are stopped.
		 *
		 * @param exclusive If {@code true}, exclusive mode is used, if {@code false}, inclusive mode is used.
		 * @return If exclusive mode is used, returns if any motor is stopped, in inclusive mode returns if all motors are stooped.
		 */
		protected boolean travel_isStopped(boolean exclusive) {
			if (exclusive)
				return !(leftMotor.isMoving() && rightMotor.isMoving()); // exclusive
			return !(leftMotor.isMoving() || rightMotor.isMoving()); // inclusive
		}

		/**
		 * Update speeds for straight line move mode.
		 */
		protected void travelRegulator_tick() {
			// calculate delta tachometers (firsly in degrees, then in millimeters)
			int left_delta_tacho = leftMotor.getTachoCount() - src_left;
			int left_delta_dist = (int) Math.round(left_delta_tacho / leftDegreesPerMillimeter);
			int right_delta_tacho = rightMotor.getTachoCount() - src_right;
			int right_delta_dist = (int) Math.round(right_delta_tacho / leftDegreesPerMillimeter);
			// if both left and right delta tachometers are below heatup limit
			if (Math.abs(left_delta_dist) < Settings.INT.REGULATOR_HEATUP_LIMIT.getValue() &&
					Math.abs(right_delta_dist) < Settings.INT.REGULATOR_HEATUP_LIMIT.getValue())
				return;
			// calculate wheels offset
			int delta_dist_diff = right_delta_dist - left_delta_dist;
			// if the error is too small
			if (Math.abs(delta_dist_diff) < Settings.INT.REGULATOR_NOOP_LIMIT.getValue())
				delta_dist_diff = 0; // set it to zero

			// set speeds
			setSpeedFromDiff(delta_dist_diff);
		}

		/**
		 * Stop motors.
		 */
		protected void stopMotors() {
			leftMotor.startSynchronization();
			leftMotor.stop(true);
			rightMotor.stop(true);
			leftMotor.endSynchronization();
			waitComplete();
		}

		/**
		 * Wait until previous operation is completed.
		 */
		protected void waitComplete() {
			// just wait
			leftMotor.waitComplete();
			rightMotor.waitComplete();
			// in case of move
			while (rightMotor.isMoving() || leftMotor.isMoving()) {
				leftMotor.waitComplete();
				rightMotor.waitComplete();
			}
		}

		/**
		 * Setup regulator for turning.
		 *
		 * @param degrees Angle the robot should turn by.
		 */
		public void setupTurn(float degrees) {
			// clean up variables
			stoppedByTouch = false;
			delta_left = 0;
			delta_right = 0;
			// set stop on end
			stopEnd = true;
			// set up mode
			mode = RegulatorMode.TURN;
			// add angle to expected angle, passthrough
			angle += (argument = degrees);
		}

		/**
		 * Setup regulator for arc turning.
		 *
		 * @param degrees Angle the robot should turn by.
		 */
		public void setupArcTurn(int degrees) {
			// clean up variables
			stoppedByTouch = false;
			// set stop on end
			stopEnd = true;
			// passthrough
			argument = degrees;
			// set mode
			mode = RegulatorMode.ARC_TURN;
			// calculate tachometers
			double rot_dist = degrees / 360 * (Math.PI * trackWidth);
			if (degrees > 0) { // CCW
				delta_right = (int) Math.round(rot_dist * rightDegreesPerMillimeter);
				delta_left = 0;
			} else { // CW
				delta_left = (int) Math.round(-rot_dist * leftDegreesPerMillimeter);
				delta_right = 0;
			}
		}

		/**
		 * Setup regulator for free move in straight line.
		 *
		 * @param backward Whether the robot should go backward. If {@code false}, it will go forward.
		 * @param stop     Whether regulator should stop motors after touch press.
		 */
		public void setupFreeTravel(boolean backward, boolean stop) {
			// clean up variables
			stoppedByTouch = false;
			delta_left = 0;
			delta_right = 0;
			// passthrough
			stopEnd = stop;
			// set mode
			mode = RegulatorMode.FREE_TRAVEL;
			// set argument
			argument = backward ? -1 : 1;
		}

		/**
		 * Setup regulator for precise move in straight line.
		 *
		 * @param millimeters How far should the robot travel.
		 * @param ignoreTouch Whether should the regulator ignore the touch sensor pressed status.
		 */
		public void setupTravel(int millimeters, boolean ignoreTouch) {
			// clean up variables
			stoppedByTouch = false;
			// passthrough
			travel_ignoreTouch = ignoreTouch;
			// set stop on end
			stopEnd = true;
			// set mode
			mode = RegulatorMode.TRAVEL;
			// passthrough
			argument = millimeters;
			// calculate delta tacho values
			delta_left = (int) Math.round(leftDegreesPerMillimeter * millimeters);
			delta_right = (int) Math.round(rightDegreesPerMillimeter * millimeters);
		}

		/**
		 * Stop the regulator.
		 */
		public void interrupt() {
			interrupt = true;
		}

		/**
		 * Check if the regulator is running.
		 *
		 * @return {@code true} if it is running, {@code false} if it's not.
		 */
		public boolean isRunning() {
			return running;
		}
	}
}