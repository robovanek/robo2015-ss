package main.robot;

import lejos.hardware.Brick;
import lejos.hardware.BrickFinder;
import lejos.hardware.motor.EV3LargeRegulatedMotor;
import lejos.hardware.motor.EV3MediumRegulatedMotor;
import lejos.hardware.port.Port;
import lejos.robotics.RegulatedMotor;
import lejos.utility.Delay;
import main.Robotics;
import main.Settings;

import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.LinkedBlockingDeque;

/**
 * Class for interacting with the outside world - physics implementation.
 *
 * @author Jakub Vaněk {@literal vanek.jakub4@seznam.cz}
 * @version 1.1
 */
public class RealRobotics extends Robotics {
	/**
	 * Red light reflection mode.
	 */
	public static final int LIGHT_REFLECT = 0;
	/**
	 * Light receive mode.
	 */
	public static final int LIGHT_AMBIENT = 1;
	/**
	 * RGB light reflection mode.
	 * Note: current implementation throws IllegalStateException when this mode is used.
	 */
	public static final int LIGHT_RGB = 2;
	private final SuperPilot mover;
	private final RegulatedMotor magnetMotor;
	private final Brick brick;
	private final Object op_lock;
	private float forward_velocity;
	private float rotate_velocity;
	private float forward_accel;
	private float rotate_accel;
	private float gyroOffset = 0f;
	private float travelback = 0;
	private boolean stoppedByTouch = false;
	private float leftTravelled = 0f;
	private float rightTravelled = 0f;
	private BlockingDeque<Instruction> commandStream;

	/**
	 * Create new RealRobotics instance.
	 */
	public RealRobotics() {
		// get brick instance
		brick = BrickFinder.getDefault();
		// initialize sensors
		Sensors.setInstance(new Sensors(getPort(Settings.STRING.SENSOR_SONIC_PORT),
				getPort(Settings.STRING.SENSOR_GYRO_PORT),
				getPort(Settings.STRING.SENSOR_COLOR_PORT),
				getPort(Settings.STRING.SENSOR_TOUCH_PORT)));
		// calculate values
		travelback = Settings.INT.FIELD_SIZE.getValue() / 2 - Settings.INT.WHEELS_TO_HEAD.getValue();
		// initialize magnet motor
		magnetMotor = new EV3MediumRegulatedMotor(getPort(Settings.STRING.MAGNET_MOTOR_PORT));
		magnetMotor.resetTachoCount();
		magnetMotor.setSpeed(Settings.INT.MAGNET_MOTOR_SPEED.getValue());
		magnetMotor.setAcceleration(Settings.INT.MAGNET_MOTOR_ACCEL.getValue());
		magnetMotor.setStallThreshold(10, 10);
		// initialize default speeds/accelerations
		forward_accel = Settings.INT.FORWARD_ACCEL.getValue();
		forward_velocity = Settings.INT.FORWARD_SPEED.getValue();
		rotate_accel = Settings.INT.ROTATE_ACCEL.getValue();
		rotate_velocity = Settings.INT.ROTATE_SPEED.getValue();
		// initialize motors & regulator
		RegulatedMotor left = new EV3LargeRegulatedMotor(getPort(Settings.STRING.LEFT_MOTOR_PORT));
		RegulatedMotor right = new EV3LargeRegulatedMotor(getPort(Settings.STRING.RIGHT_MOTOR_PORT));
		left.resetTachoCount();
		right.resetTachoCount();
		mover = new SuperPilot(left, right,
				Settings.FLOAT.LEFT_WHEEL_DIAMETER.getValue(),
				Settings.FLOAT.RIGHT_WHEEL_DIAMETER.getValue(),
				Settings.FLOAT.TRACK_WIDTH.getValue());
		// initialize command queue
		commandStream = new LinkedBlockingDeque<>();
		// add operation lock
		op_lock = new Object();
	}

	/**
	 * Store touch press information to local variables.
	 */
	private void store_touch() {
		stoppedByTouch = mover.stoppedByTouch();
	}

	/**
	 * Store travelled distance information to local variables.
	 */
	private void store_dist() {
		leftTravelled = mover.getDistanceTravelledLeft();
		rightTravelled = mover.getDistanceTravelledRight();
	}

	/**
	 * Convert settings key-value pair into port instance.
	 *
	 * @param value Key-value pair containing port name.
	 * @return Port instance.
	 */
	private Port getPort(Settings.STRING value) {
		return brick.getPort(value.getValue());
	}

	/**
	 * Optimize command queue.
	 */
	private void compress() {
		// backup the main queue
		Queue<Instruction> oldQ = new LinkedList<>(commandStream);
		// clear the main queue
		commandStream.clear();
		// initialize persistent values
		Instruction.Op last = null;
		float arg1 = 0;
		// iterate through old list of instructions
		for (Instruction i : oldQ) {
			// if this instruction can be combined
			if (i.isSummable()) {
				// if we had this instruction in previous iteration
				if (last == i.getOpcode()) {
					arg1 += i.getArgument(); // add the argument to previous
				} else { // if we hadn't it (it's new)
					// if it wasn't null, push it
					if (last != null) {
						commandStream.add(new Instruction(last, arg1));
					}
					// init new iteration
					last = i.getOpcode();
					arg1 = i.getArgument();
				}
			} else { // it this instruction can't be combined
				// if we had a non-null instruction in last interation, push it
				if (last != null) {
					commandStream.add(new Instruction(last, arg1));
				}
				// push this instruction
				commandStream.add(i);
				// init new iteration
				last = null;
				arg1 = 0;
			}
		}
		// push leftover instruction
		if (last != null) {
			commandStream.add(new Instruction(last, arg1));
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public void flush() {
		// optimize queue
		compress();
		// declare flag
		boolean move_travelback = false;
		// consume whole command deque as classical queue
		Instruction i;
		while ((i = commandStream.pollFirst()) != null) {
			// switch on instruction operation code
			switch (i.getOpcode()) {
				case MOVE_SET_TRAVELBACK: // set travelback flag
					move_travelback = true;
					break;
				case MOVE: // move and reset travelback flag
					perform_move(i.getArgument(), true, move_travelback, true);
					move_travelback = false;
					break;
				case TURN: // rotate the robot
					perform_turn(i.getArgument(), true, true);
					break;
				case FREE_FORWARD: // start the motors
					perform_free_forward(true, true);
					break;
				case MAGNET_UP: // put the magnet up
					perform_magnet_up(i.getArgument() == 1);
					break;
				case MAGNET_DOWN: // put the magnet down
					perform_magnet_down(i.getArgument() == 1);
					break;
				case MOVE_VELOCITY: // set the straight move velocity
					forward_velocity = i.getArgument();
					break;
				case TURN_VELOCITY: // set the rotation velocity
					rotate_velocity = i.getArgument();
					break;
				case MOVE_ACCEL: // set the straight move acceleration
					forward_accel = i.getArgument();
					break;
				case TURN_ACCEL: // set the rotation acceleration
					rotate_accel = i.getArgument();
					break;
				case SLEEP: // wait
					final int sleep = (int) i.getArgument();
					if (sleep > 0)
						Delay.msDelay(sleep);
					break;
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void waitForStop() {
		synchronized (op_lock) {
			try {
				op_lock.wait();
			} catch (InterruptedException e) {
				Thread.currentThread().interrupt();
			}
		}
	}

	/**
	 * Put the magnet down.
	 *
	 * @param block Whether this operation will block or not.
	 */
	private void perform_magnet_down(boolean block) {
		magnetMotor.rotateTo(Settings.INT.MAGNET_DOWN.getValue(), !block);
	}

	/**
	 * Put the magnet up.
	 *
	 * @param block Whether this operation will block or not.
	 */
	private void perform_magnet_up(boolean block) {
		magnetMotor.rotateTo(Settings.INT.MAGNET_UP.getValue(), !block);
	}

	/**
	 * Store the touch and travelled distance values to local variables.
	 */
	private void store_state() {
		store_touch();
		store_dist();
	}

	/**
	 * Start free motor rotation.
	 *
	 * @param block Whether this operation will wait until the rotation stops.
	 */
	private void perform_free_forward(final boolean block, final boolean notify) {
		Runnable action = new Runnable() {
			@Override
			public void run() {
				// turn to correct direction
				perform_turn(0, true, false);
				// start moving & wait for move completion
				setForwardMode();
				mover.forward(false, true);
				// store state
				store_state();
				if (notify) {
					synchronized (op_lock) {
						op_lock.notifyAll();
					}
				}
			}
		};
		// start action
		if (block) {
			action.run();
		} else {
			new Thread(action).start();
		}
	}

	/**
	 * Set correct motor speed/acceleration for straight line move mode.
	 */
	private void setForwardMode() {
		mover.setAcceleration(forward_accel);
		mover.setSpeed(forward_velocity);
	}

	/**
	 * Rotate the robot.
	 * Note: if you use 0 as angle, robot will turn to direction expected by SuperPilot (sum of turns).
	 *
	 * @param angle Angle to turn by.
	 * @param block Whether this function will block or not.
	 */
	private void perform_turn(float angle, boolean block, boolean notify) {
		setRotateMode();
		mover.rotate(angle, block);
		store_state();
		if (notify) {
			synchronized (op_lock) {
				op_lock.notifyAll();
			}
		}
	}

	/**
	 * Set correct motor speed/acceleration for rotation mode.
	 */
	private void setRotateMode() {
		// calculate and set speed
		float mm_per_sec = (float) (rotate_velocity / 360 * Math.PI * Settings.FLOAT.TRACK_WIDTH.getValue());
		mover.setSpeed(mm_per_sec);
		// calculate and set acceleration
		float mm_per_sec_squared = (float) (rotate_accel / 360 * Math.PI * Settings.FLOAT.TRACK_WIDTH.getValue());
		mover.setAcceleration(mm_per_sec_squared);
	}

	/**
	 * Perform straight line move.
	 *
	 * @param distance   Distance in millimeters.
	 * @param block      Whether this function will block or not.
	 * @param travelback Whether this move is intended to be a travelback - it will ignore touch press.
	 */
	private void perform_move(final float distance, final boolean block, final boolean travelback, final boolean notify) {
		if (!travelback) { // if this is regular move
			// start sub-algorithm
			Runnable action = new MoveRescue(distance, notify);
			if (block) {
				action.run();
			} else {
				new Thread(action).start();
			}
		} else { // if this is travelback
			// start task
			Runnable task = new Runnable() {
				@Override
				public void run() {
					// set straight line move mode
					setForwardMode();
					// travelback
					mover.travel((int) distance, true, true);
					//store_state();
					if (notify) {
						synchronized (op_lock) {
							op_lock.notifyAll();
						}
					}
				}
			};
			if (block) {
				task.run();
			} else {
				new Thread(task).start();
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void moveForward(int millimeters, boolean block) {
		if (block) {
			Instruction i = new Instruction(Instruction.Op.MOVE, millimeters);
			commandStream.add(i);
		} else {
			flush();
			perform_move(millimeters, false, false, true);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void moveBackward(int millimeters, boolean block) {
		moveForward(-millimeters, block);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void startMoveForward() {
		flush();
		setForwardMode();
		mover.forward(false, false);
		// store state
		new Thread(new Runnable() {
			@Override
			public void run() {
				mover.waitForRegulatorStop();
				store_state();
				synchronized (op_lock) {
					op_lock.notifyAll();
				}
			}
		}).start();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void startMoveBackward() {
		flush();
		setForwardMode();
		mover.backward(false);
		// store state
		new Thread(new Runnable() {
			@Override
			public void run() {
				mover.waitForRegulatorStop();
				store_state();
				synchronized (op_lock) {
					op_lock.notifyAll();
				}
			}
		}).start();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void stop() {
		mover.stop(true);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void rotate(int degrees, boolean block) {
		if (block) {
			Instruction i = new Instruction(Instruction.Op.TURN, degrees);
			commandStream.add(i);
		} else {
			flush();
			perform_turn(degrees, false, true);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public float getMoveVelocity() {
		return forward_velocity;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setMoveVelocity(float mm_per_sec) {
		if (commandStream.size() == 0) {
			this.forward_velocity = mm_per_sec;
		} else {
			Instruction i = new Instruction(Instruction.Op.MOVE_VELOCITY, mm_per_sec);
			commandStream.add(i);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setMoveAcceleration(float mm_per_sec_squared) {
		if (commandStream.size() == 0) {
			this.forward_accel = mm_per_sec_squared;
		} else {
			Instruction i = new Instruction(Instruction.Op.MOVE_ACCEL, mm_per_sec_squared);
			commandStream.add(i);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public float getRotationVelocity() {
		return rotate_velocity;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setRotationVelocity(float deg_per_sec) {
		if (commandStream.size() == 0) {
			this.rotate_velocity = deg_per_sec;
		} else {
			Instruction i = new Instruction(Instruction.Op.TURN_VELOCITY, deg_per_sec);
			commandStream.add(i);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setRotationAcceleration(float deg_per_sec_squared) {
		if (commandStream.size() == 0) {
			this.rotate_accel = deg_per_sec_squared;
		} else {
			Instruction i = new Instruction(Instruction.Op.TURN_ACCEL, deg_per_sec_squared);
			commandStream.add(i);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean frontTouch() {
		flush();
		return Sensors.getInstance().measureTouch();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public float leftSonic() {
		flush();
		return Sensors.getInstance().measureDistance();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public float gyro() {
		flush();
		return Sensors.getInstance().measureAngle() - gyroOffset;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void calibrateGyro() {
		flush();
		Sensors.getInstance().calibrateGyro();
		Sensors.getInstance().measureAngle(); // set gyro to correct mode
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public float getGyroOffset() {
		return gyroOffset;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setGyroOffset(float value) {
		gyroOffset = value;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void magnetDown(boolean block) {
		if (commandStream.size() == 0) {
			perform_magnet_down(block);
		} else {
			Instruction i = new Instruction(Instruction.Op.MAGNET_DOWN, block ? 1f : 0f);
			commandStream.add(i);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void magnetUp(boolean block) {
		if (commandStream.size() == 0) {
			perform_magnet_up(block);
		} else {
			Instruction i = new Instruction(Instruction.Op.MAGNET_UP, block ? 1f : 0f);
			commandStream.add(i);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean stoppedByTouch() {
		flush();
		return stoppedByTouch;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Light detectLight() {
		flush();
		return Sensors.getInstance().measureLight();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void moveToWall(boolean block) {
		if (block) {
			// add instructions
			Instruction i1 = new Instruction(Instruction.Op.FREE_FORWARD, 0);
			Instruction i2 = new Instruction(Instruction.Op.SLEEP, Settings.INT.ALIGN_WAIT.getValue());
			Instruction i3 = new Instruction(Instruction.Op.MOVE_SET_TRAVELBACK, 0);
			Instruction i4 = new Instruction(Instruction.Op.MOVE, -travelback);
			commandStream.add(i1);
			commandStream.add(i2);
			commandStream.add(i3);
			commandStream.add(i4);
		} else {
			flush();
			new Thread(new Runnable() {
				@Override
				public void run() {
					// go forward until we crash into the wallb
					synchronized (op_lock) {
						op_lock.notifyAll();
					}
				}
			}).start();
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isMoving() {
		return mover.isMoving();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public float getDistanceTravelledLeft() {
		return leftTravelled;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void free() {
		mover.flt(true);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public float getDistanceTravelledRight() {
		return rightTravelled;
	}

	/**
	 * Instruction container
	 *
	 * @author Jakub Vaněk {@literal vanek.jakub4@seznam.cz}
	 * @version 1.0
	 */
	private static class Instruction {
		private Op opcode;
		private float argument;

		/**
		 * Default constructor
		 *
		 * @param opcode   Operation code.
		 * @param argument Operation argument. If the operation doesn't require parameter, it is ignored.
		 */
		public Instruction(Op opcode, float argument) {
			if (opcode == null)
				throw new IllegalArgumentException("null opcode is not allowed");
			this.opcode = opcode;
			this.argument = argument;
		}

		/**
		 * Get the summability of the instruction.
		 *
		 * @return Whether this instruction can be combined with other instructions with same operation code.
		 */
		public boolean isSummable() {
			return opcode.isSummable();
		}

		/**
		 * Get the argument.
		 *
		 * @return The argument value.
		 */
		public float getArgument() {
			return argument;
		}

		/**
		 * Get the operation code.
		 *
		 * @return The operation code of this instruction.
		 */
		public Op getOpcode() {
			return opcode;
		}

		/**
		 * Operation codes
		 *
		 * @author Jakub Vaněk {@literal vanek.jakub4@seznam.cz}
		 * @version 1.0
		 */
		private enum Op {
			/**
			 * Straight move
			 */
			MOVE(true),
			/**
			 * Robot rotation
			 */
			TURN(true),
			/**
			 * Free travel
			 */
			FREE_FORWARD(true),
			/**
			 * Put the magnet up
			 */
			MAGNET_UP(true),
			/**
			 * Put the magnet down
			 */
			MAGNET_DOWN(true),
			/**
			 * Set straight line move mode velocity
			 */
			MOVE_VELOCITY(false),
			/**
			 * Set turn mode velocity
			 */
			TURN_VELOCITY(false),
			/**
			 * Set straight line move mode acceleration
			 */
			MOVE_ACCEL(false),
			/**
			 * Set turn mode acceleration
			 */
			TURN_ACCEL(false),
			/**
			 * Set the temporary travelback flag
			 */
			MOVE_SET_TRAVELBACK(true),
			/**
			 * Wait
			 */
			SLEEP(true);
			private boolean summable;

			Op(boolean summable) {
				this.summable = summable;
			}

			private boolean isSummable() {
				return summable;
			}
		}
	}

	/**
	 * Move dispatch
	 *
	 * @author Jakub Vaněk {@literal vanek.jakub4@seznam.cz}
	 * @version 1.0
	 */
	private class MoveRescue implements Runnable {
		private float distance;
		private boolean notify;

		private MoveRescue(float distance, boolean notify) {
			this.distance = distance;
			this.notify = notify;
		}

		@Override
		public void run() {
			// perform direction correction
			perform_turn(0, true, false);
			// perform move
			setForwardMode();
			mover.travel(Math.round(distance), true, false);
			// store state
			store_state();
			// check angular error
			float expect = mover.getExpectedGyro();
			float real = Sensors.getInstance().measureAngle();
			float error = expect - real;
			float abs = Math.abs(error);
			if (abs > Settings.INT.TRAVEL_MAX_TURN_ERROR.getValue()) { // too big
				float travelled;
				if (error > 0) { // if there's an extra distance on the left wheel
					travelled = mover.getDistanceTravelledLeft();
				} else { // if there's an extra distance on the right wheel
					travelled = mover.getDistanceTravelledRight();
				}
				// calculate quarter of turning arc
				float bonus_arc = (float) (Settings.FLOAT.TRACK_WIDTH.getValue() * Math.PI * abs / 180);
				// calculate future position
				float now = travelled - bonus_arc + Settings.FLOAT.TRACK_WIDTH.getValue() / 2;
				// calculate fix
				float fix = distance - now;

				// travelback
				mover.travel((int) -travelback, true, true);

				// recursive call for next possible problem
				new MoveRescue(fix, false).run();
			} else { // too small
				if (mover.stoppedByTouch()) {
					// push travelback
					Instruction i1 = new Instruction(Instruction.Op.MOVE_SET_TRAVELBACK, 0);
					Instruction i2 = new Instruction(Instruction.Op.MOVE, -travelback);
					commandStream.addFirst(i2);
					commandStream.addFirst(i1);
					// queue compress
					compress();
				}
			}
			if (notify) {
				synchronized (op_lock) {
					op_lock.notifyAll();
				}
			}
		}
	}
}