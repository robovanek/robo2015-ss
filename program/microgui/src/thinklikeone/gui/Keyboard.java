package thinklikeone.gui;

import java.util.ArrayList;
import java.util.List;

/**
 * Keyboard abstraction
 *
 * @author Jakub Vaněk {@literal <vanek.jakub4@seznam.cz>}
 */
public abstract class Keyboard implements InputProvider {
	protected List<InputStack> outputs;
	protected List<Integer> ids;

	public Keyboard() {
		outputs = new ArrayList<>();
		ids = new ArrayList<>();
	}

	@Override
	public int getID(InputStack stack) {
		for (int i = 0; i < outputs.size(); i++) {
			if (outputs.get(i) == stack)
				return ids.get(i);
		}
		return -1;
	}

	@Override
	public void addToStack(InputStack listener) {
		outputs.add(listener);
		ids.add(listener.generateID());
	}

	public abstract KeyCode getKey();

	public abstract char getChar();

	public abstract boolean getPhysical();

	public abstract void setPhysical(boolean physical);

	protected void pushKeyboardEvent(KeyCode key, char ch, KeyboardEvent.Type type) {
		KeyboardEvent<KeyCode> k;
		for (int i = 0; i < outputs.size(); i++) {
			k = new KeyboardEvent<>(ids.get(i), key, type, ch);
			outputs.get(i).putEvent(k);
		}
	}
}
