package thinklikeone.gui;

/**
 * Input event provider
 *
 * @author Jakub Vaněk {@literal <vanek.jakub4@seznam.cz>}
 */
public interface InputProvider {
	/**
	 * Add event hook to this input provider.
	 *
	 * @param listener InputStack instance.
	 */
	void addToStack(InputStack listener);

	/**
	 * Switch input provider to active mode.
	 */
	void activate();

	/**
	 * Switch input provider to passive mode.
	 */
	void deactivate();

	/**
	 * Get id of this event source associated with given input stack.
	 *
	 * @return Source ID.
	 */
	int getID(InputStack stack);
}
