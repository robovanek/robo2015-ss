package thinklikeone.gui;

import java.util.List;

/**
 * Container using relative-to-origin coordinate layout system.
 *
 * @author Jakub Vaněk {@literal <vanek.jakub4@seznam.cz>}
 */
public class AbsoluteContainer extends Container implements InputListener {

	public AbsoluteContainer() {
		super();
	}

	public AbsoluteContainer(int x, int y, int width, int height, Container parent) {
		super(x, y, width, height, parent);
	}

	public AbsoluteContainer(List<Control> controls, int x, int y, int width, int height, Container parent) {
		super(controls, x, y, width, height, parent);
	}

	@Override
	public void draw(int x, int y) {
		if (controls.size() > 0) {
			int absX = getAbsoluteX();
			int absY = getAbsoluteY();
	        /*
			GraphicsLCD lcd = render.getLCD();
			lcd.setColor(GraphicsLCD.WHITE);
			lcd.fillRect(absX, absY, getWidth(), getHeight());
			lcd.setColor(GraphicsLCD.BLACK);
			*/
			for (Control c : controls) {
				c.draw(c.getX() + absX, c.getY() + absY);
			}
		}
	}

	@Override
	public void eventHandler(Event event) {
		if (focused != null) {
			focused.eventHandler(event);
		}
	}
}
