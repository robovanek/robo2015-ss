package thinklikeone.gui;

/**
 * Drawable element abstraction
 *
 * @author Jakub Vaněk {@literal <vanek.jakub4@seznam.cz>}
 */
public interface Drawable {
	/**
	 * Draw this element.
	 *
	 * @param x Top-left corner X coordinate.
	 * @param y Top-left corner Y coordinate.
	 */
	void draw(int x, int y);

	int getWidth();

	void setWidth(int w);

	int getHeight();

	void setHeight(int h);
}
