package thinklikeone.gui;

import lejos.hardware.lcd.GraphicsLCD;

import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * {@link java.awt.Component}+{@link java.awt.image.BufferedImage} I/O glue
 *
 * @author Jakub Vaněk {@literal <vanek.jakub4@seznam.cz>}
 */
public class SwingPlatform extends Platform {
	private SwingRenderer rendererImpl;
	private SwingKeyboard kbd;
	private SwingGraphicsLCD lcd;

	public SwingPlatform(Component input, Component redraw, BufferedImage destination) {
		lcd = new SwingGraphicsLCD(redraw, destination, Color.BLACK, new Color(0, 190, 18));
		rendererImpl = new SwingRenderer();
		kbd = new SwingKeyboard(input);
		this.addInputProvider(kbd);
	}

	@Override
	public thinklikeone.gui.Renderer getRenderer() {
		return rendererImpl;
	}

	public Keyboard getKeyboard() {
		return kbd;
	}


	private class SwingRenderer extends thinklikeone.gui.Renderer {
		private SwingRenderer() {
		}

		@Override
		public GraphicsLCD getLCD() {
			return lcd;
		}
	}
}