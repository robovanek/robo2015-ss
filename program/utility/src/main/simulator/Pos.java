package main.simulator;

/**
 * Wrapper for coordinates
 * Imported from TuxTARDIS
 *
 * @author Jakub Vaněk {@literal vanek.jakub4@seznam.cz}
 */
public class Pos {
	public static final Pos none = null;
	public final int x;
	public final int y;

	public Pos(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public static Pos modify(Pos orig, int orient, int count) {
		switch (orient) {
			case MapOrient.North:
				return new Pos(orig.x, orig.y - count);
			case MapOrient.East:
				return new Pos(orig.x + count, orig.y);
			case MapOrient.South:
				return new Pos(orig.x, orig.y + count);
			case MapOrient.West:
				return new Pos(orig.x - count, orig.y);
			default:
				return orig;
		}
	}

	public static Pos[] adjacent(Pos p) {
		return new Pos[]{new Pos(p.x, p.y - 1), // north
				new Pos(p.x + 1, p.y), // east
				new Pos(p.x, p.y + 1), // south
				new Pos(p.x - 1, p.y)}; // west
	}

	public static Pos[] adjacent(Pos curPos, int curOrient) {
		return new Pos[]{
				Pos.modify(curPos, curOrient, 1), // front
				Pos.modify(curPos, curOrient, -1), // back
				Pos.modify(curPos, MapOrient.edit(curOrient, -1), 1), // left
				Pos.modify(curPos, MapOrient.edit(curOrient, 1), 1) // right
		};
	}

	public static int directionOf(Pos original, Pos next) {
		if (original.x == next.x && original.y > next.y) // north
			return MapOrient.North;
		if (original.x < next.x && original.y == next.y) // east
			return MapOrient.East;
		if (original.x == next.x && original.y < next.y) // south
			return MapOrient.South;
		if (original.x > next.x && original.y == next.y) // west
			return MapOrient.West;
		return 0;
	}
}
