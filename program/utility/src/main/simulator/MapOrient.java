package main.simulator;

/**
 * Direction enumeration
 * Imported from TuxTARDIS
 *
 * @author Jakub Vaněk {@literal vanek.jakub4@seznam.cz}
 */
public class MapOrient {
	public static final int North = 0;
	public static final int East = 1;
	public static final int South = 2;
	public static final int West = 3;

	private MapOrient() {
	}

	public static int edit(int actual, int edit) {
		return (actual + edit + 4) % 4;
	}

	public static int difference(int first, int second) {
		if (first == second)
			return 0;
		int min = Math.min(first, second);
		first -= min;
		second -= min;
		if (first < second) {
			if (second == 1)
				return 1;
			if (second == 2)
				return 2;
			if (second == 3)
				return -1;
		} else {
			if (first == 1)
				return -1;
			if (first == 2)
				return 2;
			if (first == 3)
				return 1;
		}
		return 0;
	}
}
