package main.settings_update;

import main.Settings;

import javax.swing.*;
import java.io.File;
import java.io.IOException;

/**
 * Update settings file to contain all definitions.
 *
 * @author Jakub Vaněk {@literal <vanek.jakub4@seznam.cz>}
 * @version 0.1
 */
public class SettingsUpdater {
	public static void main(String[] args) throws IOException {
		JFileChooser fc = new JFileChooser();
		fc.setDialogTitle("Settings update");
		File f;
		if (fc.showOpenDialog(null) != JFileChooser.APPROVE_OPTION)
			return;
		f = fc.getSelectedFile();
		boolean created = f.createNewFile();
		String msg = created ? "Successfully created new configuration file" : "Successfully updated configuration file";
		Settings.update(f);
		JOptionPane.showMessageDialog(null, msg);
	}
}
